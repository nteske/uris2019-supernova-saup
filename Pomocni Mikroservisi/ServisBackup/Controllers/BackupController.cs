﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Net.Http.Headers;
namespace ServisBackup.Controllers
{
    [ApiController]
    [Route("api/backup")]
    public class BackupController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            HttpClient httpClient=new HttpClient();
            httpClient.GetStringAsync("http://localhost:7002/api/backup");
            httpClient.GetStringAsync("http://localhost:7003/api/backup");
            httpClient.GetStringAsync("http://localhost:7005/api/backup");
            httpClient.GetStringAsync("http://localhost:7006/api/backup");
            httpClient.GetStringAsync("http://localhost:7007/api/backup");
            httpClient.GetStringAsync("http://localhost:7008/api/backup");
            httpClient.GetStringAsync("http://localhost:7009/api/backup");
            return "Backup called!";
        }
    }
}
