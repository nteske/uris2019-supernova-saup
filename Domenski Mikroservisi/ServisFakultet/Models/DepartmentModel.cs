namespace ServisFakultet.Models
{
    public class DepartmentModel
    {
        public decimal department_id {get; set;}
        public decimal faculty_id {get; set;}
        public string department_name {get; set;}
    }
}