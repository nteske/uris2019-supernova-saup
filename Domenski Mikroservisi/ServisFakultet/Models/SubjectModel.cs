namespace ServisFakultet.Models
{
    public class SubjectModel
    {
        public decimal subject_id {get; set;}
        public decimal faculty_id {get; set;}
        public decimal department_id {get; set;}
        public decimal study_pr_id {get; set;}
        public string subject_name {get; set;}
        public string subject_description {get; set;}
        public string subject_short_name {get; set;}

    }
}