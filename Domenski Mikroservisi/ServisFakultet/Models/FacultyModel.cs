namespace ServisFakultet.Models
{
    public class FacultyModel
    {
        public decimal faculty_id {get; set;}
        public string faculty_name {get; set;}
        public string faculty_address {get; set;}
        public string faculty_city {get; set;}
        public string faculty_country {get; set;}
        public string university_name {get; set;}
        public string faculty_phone {get; set;}
        public string faculty_email {get; set;}
    }
}