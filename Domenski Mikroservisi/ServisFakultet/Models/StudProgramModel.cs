namespace ServisFakultet.Models
{
    public class StudProgramModel
    {
        public decimal study_pr_id {get; set;}
        public decimal faculty_id {get; set;}
        public decimal department_id {get; set;}
        public string study_pr_name {get; set;}
        public string study_pr_shortname {get; set;}
        public decimal study_pr_semester_num {get; set;}
    }
}