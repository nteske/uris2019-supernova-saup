using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisFakultet.Models;

namespace ServisFakultet.DataAccess
{
    public class StudProgramDB
    {
        private static StudProgramModel ReadRow(SqlDataReader reader)
        {
            StudProgramModel retVal = new StudProgramModel();
 
            retVal.study_pr_id = Convert.ToInt32(reader["study_pr_id"]);
            retVal.faculty_id = Convert.ToInt32(reader["faculty_id"]);
            retVal.department_id = Convert.ToInt32(reader["department_id"]);
            retVal.study_pr_name = reader["study_pr_name"] as string;
            retVal.study_pr_shortname = reader["study_pr_shortname"] as string;
            retVal.study_pr_semester_num = Convert.ToInt32(reader["study_pr_semester_num"]);
            return retVal;
        }

        private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["study_pr_id"];
        }

        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [ellabFACULTY].[STUDY_PROGRAM].[study_pr_id],
                    [ellabFACULTY].[STUDY_PROGRAM].[faculty_id],
                    [ellabFACULTY].[STUDY_PROGRAM].[department_id],
                    [ellabFACULTY].[STUDY_PROGRAM].[study_pr_name],
                    [ellabFACULTY].[STUDY_PROGRAM].[study_pr_shortname],
                    [ellabFACULTY].[STUDY_PROGRAM].[study_pr_semester_num]
                ";
            }
        }

        private static void FillData(SqlCommand command, StudProgramModel model)
        {   
            command.Parameters.AddWithValue("@faculty_id", model.faculty_id);
            command.Parameters.AddWithValue("@department_id", model.department_id);
            command.Parameters.AddWithValue("@study_pr_name", model.study_pr_name);
            command.Parameters.AddWithValue("@study_pr_shortname", model.study_pr_shortname);
            command.Parameters.AddWithValue("@study_pr_semester_num", model.study_pr_semester_num);
        }        

        private static object CreateLikeQueryString(string str)
        {
            return str == null ? (object)DBNull.Value : "%" + str + "%";
        }

        public static List<StudProgramModel> GetStudPrograms()
        {
            try
            {
                List<StudProgramModel> listOfStudPrograms = new List<StudProgramModel>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"SELECT * FROM [ellabFACULTY].[STUDY_PROGRAM]", AllColumnSelect);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listOfStudPrograms.Add(ReadRow(reader));
                        }
                    }
                }
 
                return listOfStudPrograms;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static StudProgramModel GetStudProgram(int study_pr_id)
        {
            try
            {
                StudProgramModel studProgram = new StudProgramModel();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"SELECT {0} FROM [ellabFACULTY].[STUDY_PROGRAM] WHERE [study_pr_id] = @study_pr_id", AllColumnSelect);
                    command.Parameters.AddWithValue("@study_pr_id", study_pr_id);

 
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            studProgram = ReadRow(reader);
                        }
                    }
                }
                return studProgram;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static String CreateStudModel(StudProgramModel studProgram)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabFACULTY].[STUDY_PROGRAM]
                        (
                            [faculty_id],
                            [department_id],
                            [study_pr_name],
                            [study_pr_shortname],
                            [study_pr_semester_num]          
                        )
                        VALUES
                        (
                            @faculty_id,
                            @department_id,
                            @study_pr_name,
                            @study_pr_shortname,
                            @study_pr_semester_num
                        )
                    ";
                    FillData(command, studProgram);
                    connection.Open();
 
                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = ReadId(reader);
                        }
                    }
 
                    return "Study program created successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static String UpdateStudProgram(StudProgramModel studProgram, int study_pr_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                    UPDATE 
                        [ellabFACULTY].[STUDY_PROGRAM] 
                    SET 
                        [faculty_id] = @faculty_id, 
                        [department_id] = @department_id,
                        [study_pr_name] = @study_pr_name,
                        [study_pr_shortname] = @study_pr_shortname,
                        [study_pr_semester_num] = @study_pr_semester_num
                    WHERE 
                        [study_pr_id] = @study_pr_id");
                    command.Parameters.AddWithValue("@study_pr_id", study_pr_id);

                    FillData(command, studProgram);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return "Study program updated successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static String DeleteStudProgram(int study_pr_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"DELETE FROM [ellabFACULTY].[STUDY_PROGRAM] WHERE [study_pr_id] = @study_pr_id");
                    command.Parameters.AddWithValue("@study_pr_id", study_pr_id);
                    connection.Open();
                    command.ExecuteNonQuery();
                }

                return "Study program deleted successfully!";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}