using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisFakultet.Models;

namespace ServisFakultet.DataAccess
{
    public class FacultyDB
    {
        private static FacultyModel ReadRow(SqlDataReader reader)
        {
            FacultyModel retVal = new FacultyModel();
 
            retVal.faculty_id = Convert.ToInt32(reader["faculty_id"]);
            retVal.faculty_name = reader["faculty_name"] as string;
            retVal.faculty_address = reader["faculty_address"] as string;
            retVal.faculty_city = reader["faculty_city"] as string;
            retVal.faculty_country = reader["faculty_country"] as string;
            retVal.university_name = reader["university_name"] as string;
            retVal.faculty_phone = reader["faculty_phone"] as string;
            retVal.faculty_email = reader["faculty_email"] as string;

            return retVal;
        }

        private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["faculty_id"];
        }

        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [ellabFACULTY].[FACULTY].[faculty_id],
                    [ellabFACULTY].[FACULTY].[faculty_name],
                    [ellabFACULTY].[FACULTY].[faculty_address],
                    [ellabFACULTY].[FACULTY].[faculty_city],
                    [ellabFACULTY].[FACULTY].[faculty_country],
                    [ellabFACULTY].[FACULTY].[university_name],
                    [ellabFACULTY].[FACULTY].[faculty_phone],
                    [ellabFACULTY].[FACULTY].[faculty_email]
                ";
            }
        }

        private static void FillData(SqlCommand command, FacultyModel model)
        {   
            //command.Parameters.AddWithValue("@faculty_id", model.faculty_id);
            command.Parameters.AddWithValue("@faculty_name", model.faculty_name);
            command.Parameters.AddWithValue("@faculty_address", model.faculty_address);
            command.Parameters.AddWithValue("@faculty_city", model.faculty_city);
            command.Parameters.AddWithValue("@faculty_country", model.faculty_country);
            command.Parameters.AddWithValue("@university_name", model.university_name);
            command.Parameters.AddWithValue("@faculty_phone", model.faculty_phone);
            command.Parameters.AddWithValue("@faculty_email", model.faculty_email);
        }      

        private static object CreateLikeQueryString(string str)
        {
            return str == null ? (object)DBNull.Value : "%" + str + "%";
        }

        public static List<FacultyModel> GetFaculties()
        {
            try
            {
                List<FacultyModel> listOfFaculties = new List<FacultyModel>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"SELECT * FROM [ellabFACULTY].[FACULTY]", AllColumnSelect);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listOfFaculties.Add(ReadRow(reader));
                        }
                    }
                }
 
                return listOfFaculties;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static FacultyModel GetFaculty(int faculty_id)
        {
            try
            {
                FacultyModel faculty = new FacultyModel();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"SELECT {0} FROM [ellabFACULTY].[FACULTY] WHERE [faculty_id] = @faculty_id", AllColumnSelect);
                    command.Parameters.AddWithValue("@faculty_id", faculty_id);
 
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            faculty = ReadRow(reader);
                        }
                    }
                }
                return faculty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String CreateFaculty(FacultyModel faculty)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabFACULTY].[FACULTY]
                        (
                            [faculty_name],
                            [faculty_address],
                            [faculty_city],
                            [faculty_country],
                            [university_name],
                            [faculty_phone],
                            [faculty_email]                
                        )
                        VALUES
                        (
                            @faculty_name,
                            @faculty_address,
                            @faculty_city,
                            @faculty_country,
                            @university_name,
                            @faculty_phone,
                            @faculty_email
                        )
                    ";
                    FillData(command, faculty);
                    connection.Open();
 
                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = ReadId(reader);
                        }
                    }
 
                    return "Faculty created successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String UpdateFaculty(FacultyModel faculty, int faculty_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        UPDATE
                            [ellabFACULTY].[FACULTY]
                        SET
                            [faculty_name] = @faculty_name,
                            [faculty_address] = @faculty_address,
                            [faculty_city] = @faculty_city,
                            [faculty_country] = @faculty_country,
                            [university_name] = @university_name,
                            [faculty_phone] = @faculty_phone,
                            [faculty_email] = @faculty_email
                        WHERE
                            [faculty_id] = @faculty_id
                    ");
                    command.Parameters.AddWithValue("@faculty_id", faculty_id);

                    FillData(command, faculty);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return "Faculty updated successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static String DeleteFaculty(int faculty_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"DELETE FROM [ellabFACULTY].[FACULTY] WHERE [faculty_id] = @faculty_id");
                    command.Parameters.AddWithValue("@faculty_id", faculty_id);
                    connection.Open();
                    command.ExecuteNonQuery();
                }

                return "Faculty deleted successfully!";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
    }
}