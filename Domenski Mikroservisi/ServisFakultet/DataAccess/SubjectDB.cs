using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisFakultet.Models;

namespace ServisFakultet.DataAccess
{
    public class SubjectDB
    {
        private static SubjectModel ReadRow(SqlDataReader reader)
        {
            SubjectModel retVal = new SubjectModel();
 
            retVal.subject_id = Convert.ToInt32(reader["subject_id"]);
            retVal.faculty_id = Convert.ToInt32(reader["faculty_id"]);
            retVal.department_id = Convert.ToInt32(reader["department_id"]);
            retVal.study_pr_id = Convert.ToInt32(reader["study_pr_id"]);
            retVal.subject_name = reader["subject_name"] as string;
            retVal.subject_description = reader["subject_description"] as string;
            retVal.subject_short_name = reader["subject_short_name"] as string;

            return retVal;
        }

        private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["subject_id"];
        }

        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [ellabFACULTY].[SUBJECT].[subject_id],
                    [ellabFACULTY].[SUBJECT].[faculty_id],
                    [ellabFACULTY].[SUBJECT].[department_id],
                    [ellabFACULTY].[SUBJECT].[study_pr_id],
                    [ellabFACULTY].[SUBJECT].[subject_name],
                    [ellabFACULTY].[SUBJECT].[subject_description],
                    [ellabFACULTY].[SUBJECT].[subject_short_name]
                ";
            }
        }

        private static void FillData(SqlCommand command, SubjectModel model)
        {   
            command.Parameters.AddWithValue("@faculty_id", model.faculty_id);
            command.Parameters.AddWithValue("@department_id", model.department_id);
            command.Parameters.AddWithValue("@study_pr_id", model.study_pr_id);
            command.Parameters.AddWithValue("@subject_name", model.subject_name);
            command.Parameters.AddWithValue("@subject_description", model.subject_description);
            command.Parameters.AddWithValue("@subject_short_name", model.subject_short_name);
        }      

        private static object CreateLikeQueryString(string str)
        {
            return str == null ? (object)DBNull.Value : "%" + str + "%";
        }

        public static List<SubjectModel> GetSubjects()
        {
            try
            {
                List<SubjectModel> listOfSubjects = new List<SubjectModel>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"SELECT * FROM [ellabFACULTY].[SUBJECT]", AllColumnSelect);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listOfSubjects.Add(ReadRow(reader));
                        }
                    }
                }
 
                return listOfSubjects;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static SubjectModel GetSubject(int subject_id)
        {
            try
            {
                SubjectModel subject = new SubjectModel();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"SELECT {0} FROM [ellabFACULTY].[SUBJECT] WHERE [subject_id] = @subject_id", AllColumnSelect);
                    command.Parameters.AddWithValue("@subject_id", subject_id);

 
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            subject = ReadRow(reader);
                        }
                    }
                }
                return subject;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static List<SubjectModel> GetByString(String pretraziPo)
        {
            try
            {
                List<SubjectModel> subjects = new List<SubjectModel>();
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    Console.WriteLine(pretraziPo);

                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       SELECT * FROM [ellabFACULTY].[SUBJECT]
                    ";
                    //command.Parameters.AddWithValue("@pretraziPo", pretraziPo);

                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if(ReadRow(reader).subject_name.Contains(pretraziPo))
                                subjects.Add(ReadRow(reader));
                            else if(ReadRow(reader).subject_description.Contains(pretraziPo))
                                subjects.Add(ReadRow(reader));
                            else if(ReadRow(reader).subject_description.Contains(pretraziPo))
                                subjects.Add(ReadRow(reader));
                            else 
                                continue;
                        }
                    }
                    Console.WriteLine(subjects.Count);
                    return subjects;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static String CrearteSubject(SubjectModel subject)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabFACULTY].[SUBJECT]
                        (
                            [faculty_id],
                            [department_id],
                            [study_pr_id],
                            [subject_name],
                            [subject_description],
                            [subject_short_name]         
                        )
                        VALUES
                        (
                            @faculty_id,
                            @department_id,
                            @study_pr_id,
                            @subject_name,
                            @subject_description,
                            @subject_short_name
                        )
                    ";

                    FillData(command, subject);
                    connection.Open();
 
                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = ReadId(reader);
                        }
                    }
 
                    return "Subject created successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static String UpdateSubject(SubjectModel subject, int subject_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                    UPDATE 
                        [ellabFACULTY].[SUBJECT] 
                    SET 
                        [faculty_id] = @faculty_id, 
                        [department_id] = @department_id,
                        [study_pr_id] = @study_pr_id,
                        [subject_name] = @subject_name,
                        [subject_description] = @subject_description,
                        [subject_short_name] = @subject_short_name
                    WHERE 
                        [subject_id] = @subject_id");
                    command.Parameters.AddWithValue("@subject_id", subject_id);

                    FillData(command, subject);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return "Subject program updated successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    public static String DeleteSubject(int subject_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"DELETE FROM [ellabFACULTY].[SUBJECT] WHERE [subject_id] = @subject_id");
                    command.Parameters.AddWithValue("@subject_id", subject_id);
                    connection.Open();
                    command.ExecuteNonQuery();
                }

                return "Subject deleted successfully!";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
