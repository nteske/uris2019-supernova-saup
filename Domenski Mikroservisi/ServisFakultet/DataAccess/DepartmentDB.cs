using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisFakultet.Models;

namespace ServisFakultet.DataAccess
{
    public class DepartmentDB
    {
        private static DepartmentModel ReadRow(SqlDataReader reader)
        {
            DepartmentModel retVal = new DepartmentModel();
 
            retVal.department_id = Convert.ToInt32(reader["department_id"]);
            retVal.faculty_id = Convert.ToInt32(reader["faculty_id"]);
            retVal.department_name = reader["department_name"] as string;
 
            return retVal;
        }

        private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["department_id"];
        }

        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [ellabFACULTY].[DEPARTMENT].[department_id],
                    [ellabFACULTY].[DEPARTMENT].[faculty_id],
                    [ellabFACULTY].[DEPARTMENT].[department_name]
                ";
            }
        }

        private static void FillData(SqlCommand command, DepartmentModel department)
        {   
            //command.Parameters.AddWithValue("@department_id", department.department_id);
            command.Parameters.AddWithValue("@faculty_id", department.faculty_id);
            command.Parameters.AddWithValue("@department_name", department.department_name);
        }      

        private static object CreateLikeQueryString(string str)
        {
            return str == null ? (object)DBNull.Value : "%" + str + "%";
        }

        public static List<DepartmentModel> GetDepartments()
        {
            try
            {
                List<DepartmentModel> listOfDepartments = new List<DepartmentModel>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"SELECT * FROM [ellabFACULTY].[DEPARTMENT]", AllColumnSelect);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listOfDepartments.Add(ReadRow(reader));
                        }
                    }
                }
 
                return listOfDepartments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static DepartmentModel GetDepartment(int department_id)
        {
            try
            {
                DepartmentModel department = new DepartmentModel();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"SELECT {0} FROM [ellabFACULTY].[DEPARTMENT] WHERE [department_id] = @department_id", AllColumnSelect);
                    command.Parameters.AddWithValue("@department_id", department_id);

 
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            department = ReadRow(reader);
                        }
                    }
                }
                return department;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    public static String CreateDepartment(DepartmentModel department)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabFACULTY].[DEPARTMENT]
                        (
                            [faculty_id],
                            [department_name]           
                        )
                        VALUES
                        (
                            @faculty_id,
                            @department_name
                        )
                    ";
                    FillData(command, department);
                    connection.Open();
 
                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = ReadId(reader);
                        }
                    }
 
                    return "Department created successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    public static String UpdateDepartment(DepartmentModel department, int department_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                    UPDATE 
                        [ellabFACULTY].[DEPARTMENT] 
                    SET 
                        [faculty_id] = @faculty_id, 
                        [department_name] = @department_name 
                    WHERE 
                        [department_id] = @department_id");
                    command.Parameters.AddWithValue("@department_id", department_id);

                    FillData(command, department);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return "Department updated successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    public static String DeleteDepartment(int department_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"DELETE FROM [ellabFACULTY].[DEPARTMENT] WHERE [department_id] = @department_id");
                    command.Parameters.AddWithValue("@department_id", department_id);
                    connection.Open();
                    command.ExecuteNonQuery();
                }

                return "Department deleted successfully!";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}