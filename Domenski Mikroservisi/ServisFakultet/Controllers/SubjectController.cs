using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using ServisFakultet.DataAccess;
using ServisFakultet.Models;
using Microsoft.AspNetCore.Mvc;
using ServisFakultet.Services;
using ServisFakultet.DTO;
namespace ServisFakultet.Controllers
{
    [ApiController]
    public class SubjectController : ControllerBase
    {

        private ISubjectService _subjectservice;

        public SubjectController(ISubjectService subjectService)
        {
            _subjectservice = subjectService;
        }

        [Route("api/subjects")]
        [HttpGet]
        public List<SubjectModel> GetSubjects()
        {
            var subjects = _subjectservice.GetAll();
            return subjects;
        }

        //TO DO
        [Route("api/subject/{id}")]
        [HttpGet]
        public SubjectModel GetSubject(int id)
        {
            SubjectModel subject = _subjectservice.GetPredmetById(id);
            return subject;
        }

        [Route("api/subject/static/{id}")]
        [HttpGet]
        public String GetSubjectStaticInfo(int id)
        {
            SubjectModel subject = _subjectservice.GetPredmetById(id);
            string text = "Subject name: ";
            text += subject.subject_name + "\n";
            text += "Subject description: ";
            text += subject.subject_description + "\n";
            text += "Subject short name: ";
            text += subject.subject_short_name + "\n";
           // tekst+=_userService.GetAll().ToString();
            return text;
        }

        [Route("api/subject/create")]
        [HttpPost]
        public String CreateSubject([FromBody]SubjectModel subject)
        {                  
            var text = _subjectservice.CreateSubject(subject);
            return text;
        }

        [Route("api/subject/modify/{id}")]
        [HttpPut]
        public String UpdateSubject([FromBody]SubjectModel subject, int id)
        {
            var text = _subjectservice.UpdateSubject(subject, id);
            return text;
        }

        [Route("api/subject/{id}")]
        [HttpDelete]
        public String DeleteSubject(int id)
        {
            var text = _subjectservice.DeleteSubject(id);
            return text;
        }

        [Route("api/subjectsearch")]
        [HttpGet]
        public List<SubjectModel> GetSubjectsByString([FromBody]SearchObject search)
        {
            var subjects = _subjectservice.GetSubjectsByString(search.Search);
            return subjects;
        }
    }
    
}

