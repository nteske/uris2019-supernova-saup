using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using ServisFakultet.DataAccess;
using ServisFakultet.Models;
using ServisFakultet.Services;
using Microsoft.AspNetCore.Mvc;

namespace ServisFakultet.Controllers
{
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService) {
            _departmentService = departmentService;
        }

        [Route("api/departments")]
        [HttpGet]
        public List<DepartmentModel> GetDepartments()
        {
            var departments = _departmentService.GetAll();
            return departments;
        }

        [Route("api/department/{id}")]
        [HttpGet]
        public DepartmentModel GetDepartment(int id)
        {
            var department = _departmentService.GetDepartmentById(id);
            return department;
        }

        [Route("api/department/create")]
        [HttpPost]
        public String CreateDepartment([FromBody]DepartmentModel department)
        {                  
            var text = _departmentService.CreateDepartment(department);
            return text;
        }

        [Route("api/department/modify/{id}")]
        [HttpPut]
        public String UpdateDepartment([FromBody]DepartmentModel department, int id)
        {
            var text = _departmentService.UpdateDepartment(department, id);
            return text;
        }

        [Route("api/department/{id}")]
        [HttpDelete]
        public String DeleteDepartment(int id)
        {
            var text = _departmentService.DeleteDepartment(id);
            return text;
        }

        /*[Route("api/backup")]
        [HttpGet]
        public void Backup(int id)
        {
            BackupService.Backup();
        }*/
    }
    
}

