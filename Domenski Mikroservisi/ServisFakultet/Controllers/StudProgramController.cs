using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using ServisFakultet.DataAccess;
using ServisFakultet.Models;
using ServisFakultet.Services;
using Microsoft.AspNetCore.Mvc;

namespace ServisFakultet.Controllers
{
    [ApiController]
    public class StudProgramController : ControllerBase
    {
        private IStudProgramService _studProgramService;

        public StudProgramController(IStudProgramService studProgramService) {
            _studProgramService = studProgramService;
        }

        [Route("api/studprograms")]
        [HttpGet]
        public List<StudProgramModel> GetStudPrograms()
        {
           var studPrograms = _studProgramService.GetAll();
           return studPrograms;
        }

        [Route("api/studprogram/{id}")]
        [HttpGet]
        public StudProgramModel GetStudProgram(int id)
        {
            var studProgram = _studProgramService.GetStudProgramById(id);
            return studProgram;
        }

        [Route("api/studprogram/create")]
        [HttpPost]
        public String CreateStudProgram([FromBody]StudProgramModel studProgram)
        {                  
            var text = _studProgramService.CreateStudProgram(studProgram);
            return text;
        }

        [Route("api/studprogram/modify/{id}")]
        [HttpPut]
        public String UpdateStudProgram([FromBody]StudProgramModel studProgram, int id)
        {
            var text = _studProgramService.UpdateStudProgram(studProgram, id);
            return text;
        }

        [Route("api/studprogram/{id}")]
        [HttpDelete]
        public String DeleteStudProgram(int id)
        {
            var text = _studProgramService.DeleteStudProgram(id);
            return text;
        }
    }
}