using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using ServisFakultet.DataAccess;
using ServisFakultet.Models;
using Microsoft.AspNetCore.Mvc;
using ServisFakultet.Services;

namespace ServisFakultet.Controllers
{
    [ApiController]
    public class FacultyController : ControllerBase
    {
        private IFacultyService _facultyservice;

        public FacultyController(IFacultyService facultyService)
        {
            _facultyservice = facultyService;
        }

        //DONE
        [Route("api/faculties")]
        [HttpGet]
        public List<FacultyModel> GetFaculties()
        {
            List<FacultyModel> faculties = _facultyservice.GetAll();
            return faculties;
        }

        //DONE
        [Route("api/faculty/{id}")]
        [HttpGet]
        public FacultyModel GetFaculty(int id)
        {
            FacultyModel faculty = _facultyservice.GetOneById(id);
            return faculty;
        }


        //DONE
        [Route("api/faculty/create")]
        [HttpPost]
        public String CreateFaculty([FromBody]FacultyModel faculty)
        {                  
            var text = _facultyservice.CreateFaculty(faculty);
            return text;
        }

        [Route("api/faculty/modify/{id}")]
        [HttpPut]
        public String UpdateFaculty([FromBody]FacultyModel faculty, int id)
        {
            var text = _facultyservice.UpdateFaculty(faculty, id);
            return text;
        }

        [Route("api/faculty/{id}")]
        [HttpDelete]
        public String DeleteFaculty(int id)
        {
           var text = _facultyservice.DeleteFaculty(id);
           return text;
        }
    }
    
}

