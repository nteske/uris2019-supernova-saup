using ServisFakultet.DataAccess;
using ServisFakultet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;

namespace ServisFakultet.Services
{
    public interface IFacultyService 
    {
        List<FacultyModel> GetAll();
        FacultyModel GetOneById(int id);
        String CreateFaculty(FacultyModel faculty);
        String UpdateFaculty(FacultyModel faculty, int id);
        String DeleteFaculty(int id);
    }
    public class FacultyService : IFacultyService
    {
        public List<FacultyModel> GetAll()
        {
            if(FacultyDB.GetFaculties().Count == 0)
            {
                return null;
            }
            return FacultyDB.GetFaculties();
        }

        public FacultyModel GetOneById(int id)
        {
            if(FacultyDB.GetFaculty(id) is null)
                return null;
            return FacultyDB.GetFaculty(id);
        }

        public String CreateFaculty(FacultyModel faculty)
        {                  
            return FacultyDB.CreateFaculty(faculty);
        }

        public String UpdateFaculty(FacultyModel faculty, int id)
        {
            return FacultyDB.UpdateFaculty(faculty, id);
        }

        public String DeleteFaculty(int id)
        {
            return FacultyDB.DeleteFaculty(id);
        }

    }
}