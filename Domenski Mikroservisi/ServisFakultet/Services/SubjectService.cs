using ServisFakultet.DataAccess;
using ServisFakultet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;

namespace ServisFakultet.Services
{
    public interface ISubjectService 
    {       
        List<SubjectModel> GetAll();
        SubjectModel GetPredmetById(int id);
        String CreateSubject(SubjectModel subject);
        String UpdateSubject(SubjectModel subject, int id);
        String DeleteSubject(int id);
        List<SubjectModel> GetSubjectsByString(String search);
    }
    public class SubjectService : ISubjectService
    {
        public List<SubjectModel> GetAll()
        {
            if(SubjectDB.GetSubjects().Count == 0)
                return null;
            return SubjectDB.GetSubjects();
        }

        public SubjectModel GetPredmetById(int id)
        {
            if(SubjectDB.GetSubject(id) is null)
                return null;
            return SubjectDB.GetSubject(id);
        }

        public String CreateSubject(SubjectModel subject)
        {                  
            return SubjectDB.CrearteSubject(subject);
        }

        public String UpdateSubject(SubjectModel subject, int id)
        {
            return SubjectDB.UpdateSubject(subject, id);
        }

        public String DeleteSubject(int id)
        {
            return SubjectDB.DeleteSubject(id);
        }

        public List<SubjectModel> GetSubjectsByString(String search)
        {
            //if(SubjectDB.GetByString(search).Count == 0)
              //  return null;
            //else 
                return SubjectDB.GetByString(search);

        }
    }
}