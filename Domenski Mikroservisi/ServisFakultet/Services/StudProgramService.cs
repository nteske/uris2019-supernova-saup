using ServisFakultet.DataAccess;
using ServisFakultet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;

namespace ServisFakultet.Services
{
    public interface IStudProgramService 
    {
        List<StudProgramModel> GetAll();
        StudProgramModel GetStudProgramById(int id);
        String CreateStudProgram(StudProgramModel studProgram);
        String UpdateStudProgram(StudProgramModel studProgram, int id);
        String DeleteStudProgram(int id);
    }

    public class StudProgramService : IStudProgramService
    {
        public List<StudProgramModel> GetAll()
        {
            if(StudProgramDB.GetStudPrograms().Count == 0)
                return null;
            return StudProgramDB.GetStudPrograms();
        }

        public StudProgramModel GetStudProgramById(int id)
        {
            if(StudProgramDB.GetStudProgram(id) is null)
                return null;
            return StudProgramDB.GetStudProgram(id);
        }
        
        public String CreateStudProgram(StudProgramModel studProgram)
        {                  
            return StudProgramDB.CreateStudModel(studProgram);
        }

        public String UpdateStudProgram(StudProgramModel studProgram, int id)
        {
            return StudProgramDB.UpdateStudProgram(studProgram, id);
        }

        public String DeleteStudProgram(int id)
        {
            return StudProgramDB.DeleteStudProgram(id);
        }
    }
}