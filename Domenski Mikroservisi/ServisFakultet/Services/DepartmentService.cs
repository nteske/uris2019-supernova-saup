using ServisFakultet.DataAccess;
using ServisFakultet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;

namespace ServisFakultet.Services
{
    public interface IDepartmentService 
    {
        List<DepartmentModel> GetAll();
        DepartmentModel GetDepartmentById(int id);
        String CreateDepartment(DepartmentModel department);
        String UpdateDepartment(DepartmentModel department, int id);
        String DeleteDepartment(int id);
    }


    public class DepartmentService : IDepartmentService
    {
        public List<DepartmentModel> GetAll()
        {
            if(DepartmentDB.GetDepartments().Count == 0)
                return null;
            return DepartmentDB.GetDepartments();
        }

        public DepartmentModel GetDepartmentById(int id)
        {
            if(DepartmentDB.GetDepartment(id) is null)
                return null;
            return DepartmentDB.GetDepartment(id);
        }
        
        public String CreateDepartment(DepartmentModel department)
        {                  
            return DepartmentDB.CreateDepartment(department);
        }

        public String UpdateDepartment(DepartmentModel department, int id)
        {
            return DepartmentDB.UpdateDepartment(department, id);
        }

        public String DeleteDepartment(int id)
        {
            return DepartmentDB.DeleteDepartment(id);
        }
    }
}