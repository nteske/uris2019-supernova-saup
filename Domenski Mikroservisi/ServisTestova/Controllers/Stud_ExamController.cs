using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ServisTestova.models;
using ServisTestova.DataAccess;
using ServisTestova.services;
using System.Threading.Tasks;

namespace ServisTestova.Controllers
{
    public class Stud_ExamController : ControllerBase
    {

        private IStudExamService _studexamService;

        public Stud_ExamController(IStudExamService studexamService) {
            _studexamService = studexamService;
        }

        [Route("/getAllStudExams")]
        [HttpGet]
        public List<Stud_Exam> getAllStudExams()
        {
            var list = _studexamService.GetAllStudExams();
            return list;
        }

        [Route("/getStudExam/{id_exam}&{id_student}")]
        [HttpGet]
        public Stud_Exam GetSubject(int id_exam, int id_student)
        {
            var stud_exam = _studexamService.GetStudExam(id_exam, id_student);
            return stud_exam;
        }

        [Route("/postStudExam")]
        [HttpPost]
        public String PostStudExam([FromBody]Stud_Exam stud_exam)
        {
            var tekst = _studexamService.CreateStudExam(stud_exam);
            return tekst;
        }

        [Route("/updateStudExam/{id_exam}&{id_student}")]
        [HttpPut]
        public String UpdateStudExam([FromBody]Stud_Exam stud_exam, int id_exam, int id_student)
        {
            var tekst = _studexamService.UpdateStudExam(stud_exam, id_exam, id_student);
            return tekst;
        }

        [Route("/deleteStudExam/{id_exam}&{id_student}")]
        [HttpDelete]
        public String DeleteStudExam(int id_exam, int id_student)
        {
            var tekst = _studexamService.DeleteStudExam(id_exam, id_student);
            return tekst;
        }

        [Route("/prijaviPolaganjeTesta")]
        [HttpPost]
        public String PrijaviPolaganjeTesta([FromBody] PrijavaPolaganjaTestaModel prijavaPolaganjaTestaModel)
        {
            var id_exam = prijavaPolaganjaTestaModel.id_exam;
            var id_course = prijavaPolaganjaTestaModel.id_course;
            var id_teacher = prijavaPolaganjaTestaModel.id_teacher;
            var idstudent = Convert.ToInt32(HttpContext.Request.Headers["User_id"]);
            var tekst = _studexamService.PrijaviPolaganjeTesta(id_exam, idstudent, id_course, id_teacher);

            return tekst;
        }
    }
}