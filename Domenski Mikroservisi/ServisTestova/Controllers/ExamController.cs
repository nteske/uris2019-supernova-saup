using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ServisTestova.models;
using ServisTestova.DataAccess;
using ServisTestova.services;

namespace ServisTestova.Controllers
{
    [ApiController]
    public class ExamController : ControllerBase
    {
    
        private IExamService _examService;

        public ExamController(IExamService examService) {
            _examService = examService;
        }

        [Route("/getAllExams")]
        [HttpGet]
        public List<Exam> getAllExams()
        {
            var users = _examService.GetAll();
            return users;
        }
        
        [Route("/getExam/{id}")]
        [HttpGet]
        public Exam GetSubject(int id)
        {
            var user = _examService.GetById(id);
            return user;
        }

        [Route("/postExam")]
        [HttpPost]
        public String PostExam([FromBody]Exam exam) {
            var tekst = _examService.PostExam(exam);
            return tekst;
        }

        [Route("/updateExam/{id}")]
        [HttpPut]
        public String UpdateExam([FromBody]Exam exam, int id) {
            var tekst = _examService.UpdateExam(exam, id);
            return tekst;
        }

        [Route("/deleteExam/{id}")]
        [HttpDelete]
        public String DeleteExam(int id) {
            var tekst = _examService.DeleteExam(id);
            return tekst;
        }
    }


}