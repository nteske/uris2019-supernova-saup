using System;
using System.Collections.Generic;
using ServisTestova.models;
using System.Threading.Tasks;
using ServisTestova.DataAccess;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace ServisTestova.services
{
    public interface IStudExamService
    {
        List<Stud_Exam> GetAllStudExams();
        Stud_Exam GetStudExam(int id_exam, int id_student);
        String CreateStudExam(Stud_Exam stud_exam);
        String UpdateStudExam(Stud_Exam stud_exam, int id_exam,int id_student);
        String DeleteStudExam(int id_exam,int id_student);
        String PrijaviPolaganjeTesta(decimal id_exam, decimal id_student, decimal id_course, decimal id_teacher);
    }

    public class Stud_ExamService : IStudExamService
    {
        public List<Stud_Exam> GetAllStudExams()
        {
            return Stud_ExamDB.GetAllStudExams();
        }   

        public Stud_Exam GetStudExam(int id_exam,int id_student) {
            return Stud_ExamDB.GetStudExam(id_exam, id_student);
        }

        public String CreateStudExam(Stud_Exam stud_exam)
        {
            return Stud_ExamDB.CreateStudExam(stud_exam);
        }

        public String UpdateStudExam(Stud_Exam stud_exam, int id_exam, int id_student)
        {
            return Stud_ExamDB.UpdateStudExam(stud_exam, id_exam, id_student);
        }

        public String DeleteStudExam(int id_exam, int id_student)
        {
            return Stud_ExamDB.DeleteStudExam(id_exam, id_student);
        }

        public String PrijaviPolaganjeTesta(decimal id_exam, decimal id_student, decimal id_course, decimal id_teacher)
        {
            using(var client = new HttpClient()) 
            {
                StudCourseKey model = new StudCourseKey();
                model.course_id = id_course;
                model.student_id = id_student;
                model.teacher_id = id_teacher;
                var myContent = JsonConvert.SerializeObject(model);
                var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = client.PostAsync("http://localhost:7007/api/studcoursebyid", byteContent).Result;
                Console.WriteLine(byteContent);
               if(!response.ReasonPhrase.Equals("No Content"))
               {
                   Console.WriteLine("Response reason phrase" + response.ReasonPhrase);
                   Console.WriteLine("Response status code" + response.StatusCode);
                   DateTime now = DateTime.Now;
                   return Stud_ExamDB.PrijaviPolaganjeTesta(id_exam, id_student, now);
               } 
               else 
               {
                   Console.WriteLine("Response reason phrase" + response.ReasonPhrase);
                   Console.WriteLine("Response status code" + response.StatusCode);
                   return "Nije moguca prijava ispita.";
               }
            }
        }

    }
}