using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ServisTestova.models;
using System.Threading.Tasks;
using ServisTestova.DataAccess;

namespace ServisTestova.services
{

    public interface IExamService
    {
        List<Exam> GetAll();
        Exam GetById(int id);
        String PostExam(Exam exam);
        String UpdateExam(Exam exam, int id);
        String DeleteExam(int id);

    }
    public class ExamService : IExamService
    {
        public List<Exam> GetAll() {
            return ExamDB.GetAllExams();
        }   

        public Exam GetById(int id) {
            return ExamDB.GetExam(id);
        }

        public String PostExam(Exam exam) {
            return ExamDB.CreateExam(exam);
        }

        public String UpdateExam(Exam exam, int id) {
            return ExamDB.UpdateExam(exam, id);
        }

        public String DeleteExam(int id) {
            return ExamDB.DeleteExam(id);
        }

        
    }
}