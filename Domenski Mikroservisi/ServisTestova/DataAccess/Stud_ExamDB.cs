using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisTestova.models;

namespace ServisTestova.DataAccess
{
    public class Stud_ExamDB
    {
        private static Stud_Exam ReadRow(SqlDataReader reader) {
            Stud_Exam stud_exam = new Stud_Exam();
            stud_exam.exam_id = Convert.ToInt32(reader["exam_id"]);
            stud_exam.student_id = Convert.ToInt32(reader["student_id"]);
            stud_exam.date_of_access = (DateTime)reader["date_of_access"];
            stud_exam.check_out_date = (DateTime)reader["check_out_date"];
            stud_exam.point_score = Convert.ToInt32(reader["point_score"]);
            stud_exam.is_passed = Convert.ToBoolean(reader["is_passed"]);
            stud_exam.exam_times_taken = Convert.ToInt32(reader["exam_times_taken"]);

            return stud_exam;
        }

        private static int ReadExamId(SqlDataReader reader)
        {
            return Convert.ToInt32(reader["exam_id"]);
        }

        private static int ReadStudentId(SqlDataReader reader)
        {
            return Convert.ToInt32(reader["student_id"]);
        }

        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [Stud_Exam].[exam_id],
                    [Stud_Exam].[student_id],
                    [Stud_Exam].[date_of_access],
                    [Stud_Exam].[check_out_date],
                    [Stud_Exam].[point_score],
                    [Stud_Exam].[is_passed],
                    [Stud_Exam].[exam_times_taken]
                ";
            }
        }

        private static void FillData(SqlCommand command, Stud_Exam stud_exam)
        {
            command.Parameters.AddWithValue("@exam_id", stud_exam.exam_id);
            command.Parameters.AddWithValue("@student_id", stud_exam.student_id);
            command.Parameters.AddWithValue("@date_of_access", stud_exam.date_of_access);
            command.Parameters.AddWithValue("@check_out_date", stud_exam.check_out_date);
            command.Parameters.AddWithValue("@point_score", stud_exam.point_score);
            command.Parameters.AddWithValue("@is_passed", stud_exam.is_passed);
            command.Parameters.AddWithValue("@exam_times_taken", stud_exam.exam_times_taken);
        }

        private static object CreateLikeQueryString(string str)
        {
            return str == null ? (object)DBNull.Value : "%" + str + "%";
        }

        public static List<Stud_Exam> GetAllStudExams()
        {
            try
            {
                List<Stud_Exam> listOfStudExams = new List<Stud_Exam>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            *
                        FROM
                            [ellabEXAM].[STUD_EXAM]
                    
                    ", AllColumnSelect);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listOfStudExams.Add(ReadRow(reader));
                        }
                    }
                }
 
                return listOfStudExams;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Stud_Exam GetStudExam(int id_exam, int id_student)
        {
            try
            {
                Stud_Exam stud_exam = new Stud_Exam();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabEXAM].[Stud_Exam]
                        WHERE
                            [exam_id] = @exam_id and [student_id] = @student_id
                    ", AllColumnSelect);
 
                    command.Parameters.AddWithValue("@exam_id", id_exam);
                    command.Parameters.AddWithValue("@student_id",id_student);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            stud_exam = ReadRow(reader);
                        }
                    }
                }
                return stud_exam;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String CreateStudExam(Stud_Exam stud_exam)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabEXAM].[Stud_Exam]
                        (
                            [exam_id],
                            [student_id],
                            [date_of_access],
                            [check_out_date],
                            [point_score],
                            [is_passed],
                            [exam_times_taken]               
                        )
                        VALUES
                        (
                            @exam_id,
                            @student_id,
                            @date_of_access,
                            @check_out_date,
                            @point_score,
                            @is_passed,
                            @exam_times_taken
                        )
                        SELECT @exam_id as exam_id, @student_id as student_id
                    ";
                    FillData(command, stud_exam);
                    connection.Open();
 
                    int id_exam= 0;
                    int id_student= 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id_exam = ReadExamId(reader);
                            id_student = ReadStudentId(reader);
                        }
                    }
                    return "Stud_exam created successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String UpdateStudExam(Stud_Exam stud_exam, int exam_id, int student_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabEXAM].[Stud_Exam]
                        SET
                            [exam_id] = @exam_id,
                            [student_id] = @student_id,
                            [date_of_access] = @date_of_access,
                            [check_out_date] = @check_out_date,
                            [point_score] = @point_score,
                            [is_passed] = @is_passed,
                            [exam_times_taken] = @exam_times_taken
                        WHERE
                            [exam_id] = @exam_id and [student_id] = @student_id
                    ");
 
                    FillData(command, stud_exam);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return "Stud_exam updated successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String DeleteStudExam(int exam_id, int student_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        DELETE
                            [ellabEXAM].[Stud_Exam]
                        WHERE
                            [exam_id] = @exam_id and [student_id] = @student_id   
                    ");
 
                    command.Parameters.AddWithValue("@exam_id", exam_id);
                    command.Parameters.AddWithValue("@student_id", student_id);
                    connection.Open();
                    command.ExecuteNonQuery();

                    return "Stud_exam deleted successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public static String PrijaviPolaganjeTesta(decimal id_exam, decimal id_student, DateTime date_of_access)
        {
            try
            {
                Stud_Exam stud_Exam = new Stud_Exam();
                stud_Exam.exam_id = id_exam;
                stud_Exam.student_id = id_student;
                stud_Exam.date_of_access = date_of_access;
                stud_Exam.check_out_date = date_of_access;
                var tekst = CreateStudExam(stud_Exam);
                return tekst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


       
    }
}