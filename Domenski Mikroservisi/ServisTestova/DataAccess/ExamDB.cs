using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisTestova.models;

namespace ServisTestova.DataAccess
{
    public class ExamDB
    {
        private static Exam ReadRow(SqlDataReader reader) {
            Exam exam = new Exam();
            exam.exam_id = Convert.ToInt32(reader["exam_id"]);
            exam.course_id = Convert.ToInt32(reader["course_id"]);
            exam.teacher_id = Convert.ToInt32(reader["teacher_id"]);
            exam.test_name = reader["test_name"] as string;
            exam.exam_name = reader["exam_name"] as string;
            exam.exam_type = reader["exam_type"] as string;
            exam.exam_period = reader["exam_period"] as string;
            exam.exam_date = (DateTime)reader["exam_date"];
            exam.exam_is_required = Convert.ToBoolean(reader["exam_is_required"]);
            exam.exam_min_points = Convert.ToInt32(reader["exam_min_points"]);
            exam.exam_max_points = Convert.ToInt32(reader["exam_max_points"]);
            exam.exam_time = (DateTime)reader["exam_time"];

            return exam;
        }

        private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["exam_id"];
        }
        
        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [Exam].[exam_id],
                    [Exam].[course_id],
                    [Exam].[teacher_id],
                    [Exam].[test_name],
                    [Exam].[exam_name],
                    [Exam].[exam_type],
                    [Exam].[exam_period],
                    [Exam].[exam_date],
                    [Exam].[exam_time],
                    [Exam].[exam_is_required],
                    [Exam].[exam_min_points],
                    [Exam].[exam_max_points]
                ";
            }
        }

        private static void FillData(SqlCommand command, Exam exam)
        {
            //command.Parameters.AddWithValue("@exam_id", exam.exam_id);
            command.Parameters.AddWithValue("@course_id", exam.course_id);
            command.Parameters.AddWithValue("@teacher_id", exam.teacher_id);
            command.Parameters.AddWithValue("@test_name",  exam.test_name);
            command.Parameters.AddWithValue("@exam_name", exam.exam_name);
            command.Parameters.AddWithValue("@exam_type", exam.exam_type);
            command.Parameters.AddWithValue("@exam_period",exam.exam_period);
            command.Parameters.AddWithValue("@exam_date", exam.exam_date);
            command.Parameters.AddWithValue("@exam_is_required", exam.exam_is_required);
            command.Parameters.AddWithValue("@exam_min_points", exam.exam_min_points);
            command.Parameters.AddWithValue("@exam_max_points", exam.exam_max_points);
            command.Parameters.AddWithValue("@exam_time",  exam.exam_time);
            
        }

        private static object CreateLikeQueryString(string str)
        {
            return str == null ? (object)DBNull.Value : "%" + str + "%";
        }

        public static List<Exam> GetAllExams()
        {
            try
            {
                List<Exam> listOfExams = new List<Exam>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            *
                        FROM
                            [ellabEXAM].[EXAM]
                    
                    ", AllColumnSelect);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listOfExams.Add(ReadRow(reader));
                        }
                    }
                }
 
                return listOfExams;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Exam GetExam(int id)
        {
            try
            {
                Exam exam = new Exam();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabEXAM].[Exam]
                        WHERE
                            [exam_id] = @exam_id
                    ", AllColumnSelect);
 
                    command.Parameters.AddWithValue("@exam_id", id);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            exam = ReadRow(reader);
                        }
                    }
                }
                return exam;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String CreateExam(Exam exam)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabEXAM].[Exam]
                        (
                            [course_id],
                            [teacher_id],
                            [test_name],
                            [exam_name],
                            [exam_type],
                            [exam_period],
                            [exam_date],
                            [exam_time],
                            [exam_is_required],
                            [exam_min_points],
                            [exam_max_points]                
                        )
                        VALUES
                        (
                            @course_id,
                            @teacher_id,
                            @test_name,
                            @exam_name,
                            @exam_type,
                            @exam_period,
                            @exam_date,
                            @exam_time,
                            @exam_is_required,
                            @exam_min_points,
                            @exam_max_points
                        )
                    ";
                    FillData(command, exam);
                    connection.Open();
                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = ReadId(reader);
                        }
                    }

                    return "Exam created successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String UpdateExam(Exam exam, int exam_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabEXAM].[Exam]
                        SET
                            [course_id] = @course_id,
                            [teacher_id] = @teacher_id,
                            [test_name] = @test_name,
                            [exam_name] = @exam_name,
                            [exam_type] = @exam_type,
                            [exam_period] = @exam_period,
                            [exam_date] = @exam_date,
                            [exam_is_required] = @exam_is_required,
                            [exam_min_points] = @exam_min_points,
                            [exam_max_points] = @exam_max_points,
                            [exam_time] = @exam_time
                        WHERE
                            [exam_id] = @exam_id
                    ");
                    command.Parameters.AddWithValue("@exam_id", exam_id);
                    FillData(command, exam);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return "Exam updated successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String DeleteExam(int exam_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        DELETE
                            [ellabEXAM].[Exam]
                        WHERE
                            [exam_id] = @exam_id    
                    ");
 
                    command.Parameters.AddWithValue("@exam_id",exam_id);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return "Exam deleted successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}