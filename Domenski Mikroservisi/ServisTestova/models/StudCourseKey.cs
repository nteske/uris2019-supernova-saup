namespace ServisTestova.models
{
    public class StudCourseKey
    {
        public decimal student_id{get;set;}
        public decimal course_id{get;set;}
        public decimal teacher_id{get;set;}
    }
}