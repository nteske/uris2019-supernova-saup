using System;
namespace ServisTestova.models

{
    public class Exam
    {
        public decimal exam_id {get; set;}
        public decimal course_id {get; set;}
        public decimal teacher_id {get; set;}
        public string test_name {get; set;}
        public string exam_name {get; set;}
        public string exam_type {get; set;}
        public string exam_period {get; set;}
        public DateTime exam_date {get; set;}
        public bool exam_is_required {get; set;}
        public decimal exam_min_points {get; set;}
        public decimal exam_max_points {get; set;}
        public DateTime exam_time {get; set;}

    }
}