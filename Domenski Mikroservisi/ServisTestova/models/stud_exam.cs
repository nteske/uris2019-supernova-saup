using System;
namespace ServisTestova.models
{
    public class Stud_Exam
    {
        public decimal exam_id {get; set;}
        public decimal student_id {get; set;}
        public DateTime date_of_access {get; set;}
        public DateTime check_out_date {get; set;}
        public decimal point_score {get; set;}
        public bool is_passed {get; set;}
        public decimal exam_times_taken {get; set;}
    }
}