using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using ServisKurs.DataAccess;
using ServisKurs.Models;
using ServisKurs.Services;

namespace ServisKurs.Controllers
{
    [ApiController]
    public class TimetableController : ControllerBase
    {
        private ITimetableService _timetableService;

        public TimetableController(ITimetableService timetableService) {
            _timetableService = timetableService;
        }

        [Route("api/timetable")]
        [HttpGet]
        public List<TimetableModel> GetAllTimetables()
        {
            var timetables = _timetableService.GetAllTimetables();
            return timetables;
        }
 
        [Route("api/timetable/{id_course}&{id_teacher}")]
        [HttpGet]
        public TimetableModel GetTimetableById(decimal id_course,decimal id_teacher)
        {
            var timetable = _timetableService.GetTimetableById(id_course,id_teacher);
            return timetable;
        }
 
        [Route("api/timetable/create")]
        [HttpPost]
        public String CreateTimetable([FromBody]TimetableModel timetable)
        {                  
            var text = _timetableService.CreateTimetable(timetable);
            return text;
        }
 
        [Route("api/timetable/modify/{id_course}&{id_teacher}")]
        public String UpdateTimetable([FromBody]TimetableModel timetable, decimal id_course,decimal id_teacher)
        {
            var text = _timetableService.UpdateTimetable(timetable,id_course,id_teacher);
            return text;
        }
 
        [Route("api/timetable/delete/{id_course}&{id_teacher}")]
        [HttpDelete]
        public String DeleteTimetable(decimal id_course,decimal id_teacher)
        {
            var text = _timetableService.DeleteTimetable(id_course,id_teacher);
            return text;
        }
    }
}