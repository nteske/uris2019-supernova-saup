using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using ServisKurs.DataAccess;
using ServisKurs.Models;
using ServisKurs.Services;

namespace ServisKurs.Controllers
{
    [ApiController]
    public class CourseController : ControllerBase
    {
        private ICourseService _courseService;

        public CourseController(ICourseService courseService) {
            _courseService = courseService;
        }

        [Route("api/courses")]
        [HttpGet]
        public List<CourseModel> GetAllCourses()
        {
            var courses = _courseService.GetAllCourses();
            return courses;
        }
 
        [Route("api/courses/{id}")]
        [HttpGet]
        public CourseModel GetCourseById(int id)
        {
            var course = _courseService.GetCourseById(id);
            return course;
        }
 
        [Route("api/courses/create")]
        [HttpPost]
        public String CreateCourse([FromBody]CourseModel course)
        {                  
            var text = _courseService.CreateCourse(course);
            return text;
        }
 
        [Route("api/courses/modify/{id}")]
        [HttpPut]
        public String UpdateCourse([FromBody]CourseModel course, int id)
        {
            var text = _courseService.UpdateCourse(course, id);
            return text;
        }
 
        [Route("api/courses/delete/{id}")]
        [HttpDelete]
        public String DeleteCourse(int id)
        {
            var text = _courseService.DeleteCourse(id);
            return text;
        }
        
        [Route("api/courses/copyCourse/{id}")]
        [HttpPost]
        public String CopyCourse(int id)
        {                  
            var text = _courseService.CopyCourse(id);
            return text;
        }
        [Route("api/courses/modifyStudNum/{id}")]
        [HttpPut]
        public String UpdateStudNum([FromBody]MinMaxStud minMax, int id)
        {
            var text = _courseService.UpdateStudNum(minMax.max_student_num,minMax.min_student_num,id);
            return text;
        }

    }
}