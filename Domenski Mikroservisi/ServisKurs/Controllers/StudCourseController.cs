using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using ServisKurs.DataAccess;
using ServisKurs.Models;
using ServisKurs.Services;


namespace ServisKurs.Controllers
{
    [ApiController]
    public class StudCourseController : ControllerBase
    {
        private IStudCourseService _studCourseService;

        public StudCourseController(IStudCourseService studCourseService) {
            _studCourseService = studCourseService;
        }
        [Route("api/studcourse")]
        [HttpGet]
        public List<StudCourseModel> GetAllStudCourses()
        {
           var courses = _studCourseService.GetAllStudCourses();
           return courses;
        }
 
        [Route("api/studcoursebyid")]
        [HttpPost]
        public StudCourseModel GetStudCourseById([FromBody]StudCourseKey studcourse)
        {
            Console.WriteLine("Course " + studcourse.course_id + "Teacher " + studcourse.teacher_id + "Student "+ studcourse.student_id);
            var course = _studCourseService.GetStudCourseById(studcourse.course_id,studcourse.student_id,studcourse.teacher_id);
            Console.WriteLine("Kurs " + course);
            return course;
        }
 
        [Route("api/studcourse/create")]
        [HttpPost]
        public String CreateStudCourse([FromBody]StudCourseModel studcourse)
        {                  
            var newCourse = _studCourseService.CreateStudCourse(studcourse);
            return newCourse;
        }
 
        [Route("api/studcourse/modify/{id_course}&{id_stud}&{id_teacher}")]
        [HttpPut]
        public String UpdateStudCourse([FromBody]StudCourseModel studcourse, decimal id_course,decimal id_stud,decimal id_teacher)
        {
            var updatedCourse = _studCourseService.UpdateStudCourse(studcourse,id_course,id_course,id_teacher);
            return updatedCourse;
        }
 
        [Route("api/studcourse/delete")]
        [HttpDelete]
        public String DeleteStudCourse([FromBody]StudCourseKey studcourse)
        {
            var deletedCourse = _studCourseService.DeleteStudCourse(studcourse.course_id,studcourse.student_id,studcourse.teacher_id);
            return deletedCourse;
        }

        [Route("api/studcourse/modifyGrade")]
        [HttpPut]
        public String UpdateStudGrade([FromBody]StudCourseGrade studcoursegrade)
        {
            var teacher_id = Convert.ToInt32(HttpContext.Request.Headers["User_id"]);
            var courseGrade = _studCourseService.UpdateStudGrade(studcoursegrade,teacher_id);
            return courseGrade;
        }
    }
}
