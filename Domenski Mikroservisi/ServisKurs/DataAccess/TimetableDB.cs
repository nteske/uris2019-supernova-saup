using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisKurs.Models;

namespace ServisKurs.DataAccess
{
    public class TimetableDB
    {
         private static TimetableModel ReadRow(SqlDataReader reader)
        {
            TimetableModel retVal = new TimetableModel();
 
            retVal.course_id = Convert.ToInt32(reader["course_id"]);
            retVal.teacher_id = Convert.ToInt32(reader["teacher_id"]);
           
            retVal.lesson_day = reader["lesson_day"] as string;
            retVal.classroom = reader["classroom"] as string;
            retVal.building = reader["building"] as string;
            
            retVal.start_time = (TimeSpan)reader["start_time"];
            retVal.end_time = (TimeSpan)reader["end_time"];           
            
            return retVal;
        }


        private static int ReadCourseId(SqlDataReader reader)
        {
            return (int)reader["course_id"];
        }
        private static int ReadTeacherId(SqlDataReader reader)
        {
            return (int)reader["teacher_id"];
        }

        private static string AllColumnSelect
        {
            get
            {   return @"
                    [ellabCOURSE].[TIMETABLE].[course_id],
                    [ellabCOURSE].[TIMETABLE].[teacher_id],
                    [ellabCOURSE].[TIMETABLE].[lesson_day],
                    [ellabCOURSE].[TIMETABLE].[classroom],
                    [ellabCOURSE].[TIMETABLE].[building],
                    [ellabCOURSE].[TIMETABLE].[start_time],
                    [ellabCOURSE].[TIMETABLE].[end_time]
                ";
            }
        }

        private static void FillData(SqlCommand command, TimetableModel timetable)
        {
            command.Parameters.AddWithValue("@course_id", timetable.course_id);
            command.Parameters.AddWithValue("@teacher_id", timetable.teacher_id);
            command.Parameters.AddWithValue("@lesson_day",  timetable.lesson_day);
            command.Parameters.AddWithValue("@classroom",  timetable.classroom);
            command.Parameters.AddWithValue("@building", timetable.building);
            command.Parameters.AddWithValue("@start_time", timetable.start_time);
            command.Parameters.AddWithValue("@end_time", timetable.end_time);
       }


       public static List<TimetableModel> GetAllTimetables()
        {
            try
            {
                List<TimetableModel> retVal = new List<TimetableModel>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT * FROM [it1g2016].[ellabCOURSE].[TIMETABLE]             
                    ", AllColumnSelect);
                   
                  
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(ReadRow(reader));
                        }
                    }
                }
 
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static TimetableModel GetTimetableById(decimal course_id,decimal teacher_id)
        {
            try
            {
                TimetableModel retVal = new TimetableModel();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabCOURSE].[TIMETABLE]
                        WHERE
                            [course_id] = @course_id AND [teacher_id]=@teacher_id
                    ", AllColumnSelect);
 
                    command.Parameters.AddWithValue("@course_id", course_id);
                    command.Parameters.AddWithValue("@teacher_id", teacher_id);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal = ReadRow(reader);
                            
                        }
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String CreateTimetable(TimetableModel timetable)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabCOURSE].[TIMETABLE]
                        (
                            [course_id],
                            [teacher_id],
                            [lesson_day],
                            [classroom],
                            [building],
                            [start_time],
                            [end_time]                           
                        )
                        VALUES
                        (
                            @course_id,
                            @teacher_id,
                            @lesson_day,
                            @classroom,
                            @building,
                            @start_time,
                            @end_time 
                        )
                       
                        SELECT @course_id as course_id,  @teacher_id as teacher_id
                    ";
                    FillData(command, timetable);
                    connection.Open();
 
                    int course_id = 0;
                    int teacher_id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            course_id = ReadCourseId(reader);                            
                            teacher_id = ReadTeacherId(reader);
                        }
                    }
                   
 
                    return "Timetable created successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static String UpdateTimetable(TimetableModel timetable,decimal course_id,decimal teacher_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabCOURSE].[TIMETABLE]
                        SET
                            [course_id]=@course_id,
                            [teacher_id]=@teacher_id,
                            [lesson_day]=@lesson_day,
                            [classroom]=@classroom,
                            [building]=@building,
                            [start_time]=@start_time,
                            [end_time]=@end_time
                                
                        WHERE
                            [course_id] = @course_id AND [teacher_id]=@teacher_id
                    ");
                   //  command.Parameters.AddWithValue("@course_id", course_id);
                    // command.Parameters.AddWithValue("@teacher_id", teacher_id);
                     
                    FillData(command, timetable);
                    connection.Open();
                    command.ExecuteNonQuery();
                   
                    return  "Timetable updated successfully"; 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static String DeleteTimetable(decimal course_id,decimal teacher_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        DELETE FROM
                            [ellabCOURSE].[TIMETABLE]
                        WHERE
                            [course_id] = @course_id  AND [teacher_id]=@teacher_id   
                    ");
 
                    command.Parameters.AddWithValue("@course_id", course_id);
                    command.Parameters.AddWithValue("@teacher_id", teacher_id);
                    connection.Open();
                    command.ExecuteNonQuery();

                    return "Timetable deleted successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
    }
}