using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisKurs.Models;

namespace ServisKurs.DataAccess
{
    public class StudCourseDB
    {
         private static StudCourseModel ReadRow(SqlDataReader reader)
        {
            StudCourseModel retVal = new StudCourseModel();
 
            retVal.student_id = Convert.ToInt32(reader["student_id"]);
            retVal.course_id = Convert.ToInt32(reader["course_id"]);
            retVal.teacher_id = Convert.ToInt32(reader["teacher_id"]);
            retVal.enrolment_date = (DateTime)reader["enrolment_date"];
            retVal.quit_date = (DateTime)reader["quit_date"];
            retVal.points = Convert.ToInt32(reader["points"]);
            retVal.grade = Convert.ToInt32(reader["grade"]); 
            retVal.active = Convert.ToBoolean(reader["active"]);
            retVal.team_role = reader["team_role"] as string;
            retVal.team_date_in = (DateTime)reader["team_date_in"];
            retVal.team_date_out = (DateTime)reader["team_date_out"];           
            
            return retVal;
        }


        private static int ReadCourseId(SqlDataReader reader)
        {
            return (int)reader["course_id"];
        }
        private static int ReadStudentId(SqlDataReader reader)
        {
            return (int)reader["student_id"];
        }
        private static int ReadTeacherId(SqlDataReader reader)
        {
            return (int)reader["teacher_id"];
        }

        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [ellabCOURSE].[STUD_COURSE].[student_id],
                    [ellabCOURSE].[STUD_COURSE].[course_id],
                    [ellabCOURSE].[STUD_COURSE].[teacher_id],
                    [ellabCOURSE].[STUD_COURSE].[enrolment_date],
                    [ellabCOURSE].[STUD_COURSE].[quit_date],
                    [ellabCOURSE].[STUD_COURSE].[points],
                    [ellabCOURSE].[STUD_COURSE].[grade],
                    [ellabCOURSE].[STUD_COURSE].[active],
                    [ellabCOURSE].[STUD_COURSE].[team_role],
                    [ellabCOURSE].[STUD_COURSE].[team_date_in],
                    [ellabCOURSE].[STUD_COURSE].[team_date_out]

                ";
            }
        }

        private static void FillData(SqlCommand command, StudCourseModel studCourse)
        {
            command.Parameters.AddWithValue("@student_id", studCourse.student_id);
            command.Parameters.AddWithValue("@course_id", studCourse.course_id);
            command.Parameters.AddWithValue("@teacher_id", studCourse.teacher_id);
            command.Parameters.AddWithValue("@enrolment_date",  studCourse.enrolment_date);
            command.Parameters.AddWithValue("@quit_date",  studCourse.quit_date);
            command.Parameters.AddWithValue("@points", studCourse.points);
            command.Parameters.AddWithValue("@grade", studCourse.grade);
            command.Parameters.AddWithValue("@active", studCourse.active);
            command.Parameters.AddWithValue("@team_role", studCourse.team_role);
            command.Parameters.AddWithValue("@team_date_in", studCourse.team_date_in);
            command.Parameters.AddWithValue("@team_date_out", studCourse.team_date_out);
       }


       public static List<StudCourseModel> GetAllStudCourses()
        {
            try
            {
                List<StudCourseModel> retVal = new List<StudCourseModel>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            *
                        FROM
                            [ellabCOURSE].[STUD_COURSE]
                        
                    ", AllColumnSelect);
                   
                  
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(ReadRow(reader));
                        }
                    }
                }
 
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static StudCourseModel GetStudCourseById(decimal course_id,decimal student_id,decimal teacher_id)
        {
            try
            {
                StudCourseModel retVal = new StudCourseModel();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabCOURSE].[STUD_COURSE]
                        WHERE
                            [course_id] = @course_id AND [student_id]=@student_id AND [teacher_id]=@teacher_id
                    ", AllColumnSelect);
 
                    command.Parameters.AddWithValue("@course_id", course_id);
                    command.Parameters.AddWithValue("@student_id", student_id);
                    command.Parameters.AddWithValue("@teacher_id", teacher_id);
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal = ReadRow(reader);
                        }
                    }
                }
                if(retVal.course_id==0) {
                    return null;
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String CreateStudCourse(StudCourseModel stud_course)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabCOURSE].[STUD_COURSE]
                        (
                            [student_id],
                            [course_id],
                            [teacher_id],
                            [enrolment_date],
                            [quit_date],
                            [points],
                            [grade],
                            [active],
                            [team_role],
                            [team_date_in],
                            [team_date_out]                           
                        )
                        VALUES
                        (
                            @student_id,
                            @course_id,
                            @teacher_id,
                            @enrolment_date,
                            @quit_date,
                            @points,
                            @grade,
                            @active,
                            @team_role,
                            @team_date_in,
                            @team_date_out 
                        )
                       
                        
                    ";
                    FillData(command, stud_course);
                    connection.Open();
 
                    int course_id = 0;
                    int student_id = 0;
                    int teacher_id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            course_id = ReadCourseId(reader);                            
                            student_id = ReadStudentId(reader);
                            teacher_id = ReadTeacherId(reader);
                        }
                    }
                   
 
                    return "StudCourse created successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static String UpdateStudCourse(StudCourseModel stud_course,decimal course_id,decimal student_id,decimal teacher_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        UPDATE
                            [ellabCOURSE].[STUD_COURSE]
                        SET
                            [course_id]=@course_id,
                            [student_id]=@student_id,
                            [teacher_id]=@teacher_id,
                            [enrolment_date]=@enrolment_date,
                            [quit_date]=@quit_date,
                            [points]=@points,
                            [grade]=@grade,
                            [active]=@active,
                            [team_role]=@team_role,
                            [team_date_in]=@team_date_in,
                            [team_date_out]=@team_date_out                                
                        WHERE
                            [course_id]=@course_id AND [student_id]=@student_id AND [teacher_id]=@teacher_id
                    ");
                    FillData(command, stud_course);
                    connection.Open();
                    command.ExecuteNonQuery();
                   
                    return "StudCourse updated successfully"; 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static String DeleteStudCourse(decimal course_id,decimal student_id,decimal teacher_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabCOURSE].[STUD_COURSE]
                        SET
                            [active] = 0
                        WHERE
                            [course_id] = @course_id AND [student_id]=@student_id AND [teacher_id]=@teacher_id   
                    ");
 
                    command.Parameters.AddWithValue("@student_id", student_id);
                    command.Parameters.AddWithValue("@course_id", course_id);
                    command.Parameters.AddWithValue("@teacher_id", teacher_id);
                    connection.Open();
                    command.ExecuteNonQuery();

                    return "StudCourse deleted successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public static String UpdateGrade(StudCourseGrade studGrade,int id_teacher,decimal grade)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        UPDATE
                            [ellabCOURSE].[STUD_COURSE]
                        SET
                            [points]=@points,
                            [grade]=@grade                                                           
                        WHERE
                            [course_id]=@course_id AND [student_id]=@student_id AND [teacher_id]=@teacher_id
                    ");
                    command.Parameters.AddWithValue("@points", studGrade.points);
                    command.Parameters.AddWithValue("@grade", grade);
                    command.Parameters.AddWithValue("@course_id", studGrade.course_id);
                    command.Parameters.AddWithValue("@student_id", studGrade.student_id);
                    command.Parameters.AddWithValue("@teacher_id", id_teacher);
                    
                    connection.Open();
                    command.ExecuteNonQuery();
                   
                    return "Student's grade is "+grade; 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}