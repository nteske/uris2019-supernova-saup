using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisKurs.Models;

namespace ServisKurs.DataAccess
{
    public class CourseDB
    {
        private static CourseModel ReadRow(SqlDataReader reader)
        {
            CourseModel retVal = new CourseModel();
 
            retVal.course_id = Convert.ToInt32(reader["course_id"]);
            retVal.subject_id = Convert.ToInt32(reader["subject_id"]);
            retVal.faculty_id = Convert.ToInt32(reader["faculty_id"]);
            retVal.study_pr_id = Convert.ToInt32(reader["study_pr_id"]);
            retVal.department_id = Convert.ToInt32(reader["department_id"]);
              
                       
            retVal.course_name = reader["course_name"] as string;
            retVal.course_study_type = reader["course_study_type"] as string;
            retVal.course_school_year =Convert.ToInt32(reader["course_school_year"]);
            retVal.course_year = Convert.ToInt32(reader["course_year"]);
            retVal.course_semester = Convert.ToInt32(reader["course_semester"]);
            retVal.course_ESPB = Convert.ToInt32(reader["course_ESPB"]);
            retVal.max_student_num = Convert.ToInt32(reader["max_student_num"]);
            retVal.min_student_num = Convert.ToInt32(reader["min_student_num"]);
                      
            retVal.currently_active = Convert.ToBoolean(reader["currently_active"]);
 
            retVal.course_create_date = (DateTime)reader["course_create_date"];
            retVal.course_expiry_date = (DateTime)reader["course_expiry_date"];
            retVal.course_enrollment_policy = reader["course_enrollment_policy"] as string;
            Console.WriteLine(retVal.course_id);
            return retVal;
        }
        private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["course_id"];
        }
        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [ellabCOURSE].[COURSE].[course_id],
                    [ellabCOURSE].[COURSE].[subject_id],
                    [ellabCOURSE].[COURSE].[faculty_id],
                    [ellabCOURSE].[COURSE].[study_pr_id],
                    [ellabCOURSE].[COURSE].[department_id],
                    [ellabCOURSE].[COURSE].[course_name],
                    [ellabCOURSE].[COURSE].[course_study_type],
                    [ellabCOURSE].[COURSE].[course_school_year],
                    [ellabCOURSE].[COURSE].[course_year],
                    [ellabCOURSE].[COURSE].[course_semester],
                    [ellabCOURSE].[COURSE].[course_ESPB],
                    [ellabCOURSE].[COURSE].[max_student_num],
                    [ellabCOURSE].[COURSE].[min_student_num],
                    [ellabCOURSE].[COURSE].[currently_active],
                    [ellabCOURSE].[COURSE].[course_create_date],
                    [ellabCOURSE].[COURSE].[course_expiry_date],
                   [ellabCOURSE].[COURSE].[course_enrollment_policy]


                ";
            }
        }
        private static void FillData(SqlCommand command, CourseModel course)
        {
           // command.Parameters.AddWithValue("@course_id", course.course_id);
            command.Parameters.AddWithValue("@subject_id",  course.subject_id);
            command.Parameters.AddWithValue("@faculty_id",  course.faculty_id);
            command.Parameters.AddWithValue("@study_pr_id",  course.study_pr_id);
            command.Parameters.AddWithValue("@department_id", course.department_id);
            command.Parameters.AddWithValue("@course_name", course.course_name);
            command.Parameters.AddWithValue("@course_study_type", course.course_study_type);
            command.Parameters.AddWithValue("@course_school_year", course.course_school_year);
            command.Parameters.AddWithValue("@course_year", course.course_year);
            command.Parameters.AddWithValue("@course_semester", course.course_semester);
            command.Parameters.AddWithValue("@course_ESPB", course.course_ESPB);
            command.Parameters.AddWithValue("@max_student_num", course.max_student_num);
            command.Parameters.AddWithValue("@min_student_num", course.min_student_num);
            command.Parameters.AddWithValue("@currently_active",course.currently_active);
            command.Parameters.AddWithValue("@course_create_date",course.course_create_date);
            command.Parameters.AddWithValue("@course_expiry_date", course.course_expiry_date);      
            command.Parameters.AddWithValue("@course_enrollment_policy", course.course_enrollment_policy);
            
            
        }
        private static object CreateLikeQueryString(string str)
        {
            return str == null ? (object)DBNull.Value : "%" + str + "%";
        }
        public static List<CourseModel> GetAllCourses()
        {
            try
            {
                List<CourseModel> retVal = new List<CourseModel>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT * FROM [ellabCOURSE].[COURSE]", AllColumnSelect);
                   
                  
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(ReadRow(reader));
                            
                        }
                    }
                }

                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static CourseModel GetCourse(decimal course_id)
        {
            try
            {
                CourseModel retVal = new CourseModel();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabCOURSE].[COURSE]
                        WHERE
                            [course_id] = @course_id
                    ", AllColumnSelect);
 
                    command.Parameters.AddWithValue("@course_id", course_id);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal = ReadRow(reader);
                            
                        }
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static String CreateCourse(CourseModel course)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabCOURSE].[COURSE]
                        (
                            [subject_id],
                            [faculty_id],
                            [study_pr_id],
                            [department_id],
                            [course_name],
                            [course_study_type],
                            [course_school_year],
                            [course_year],
                            [course_semester],
                            [course_ESPB],
                            [max_student_num],
                            [min_student_num],
                            [currently_active],
                            [course_create_date],
                            [course_expiry_date],
                            [course_enrollment_policy]                           
                        )
                        VALUES
                        (
                            @subject_id,
                            @faculty_id,
                            @study_pr_id,
                            @department_id,
                            @course_name,
                            @course_study_type,
                            @course_school_year,
                            @course_year,
                            @course_semester,
                            @course_ESPB,
                            @max_student_num,
                            @min_student_num,
                            @currently_active,
                            @course_create_date,
                            @course_expiry_date,
                            @course_enrollment_policy
                        )
                                             
                    ";
                    
                    FillData(command, course);
                    connection.Open();
 
                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = ReadId(reader);
                        }
                    }
                   
 
                    return "Course created successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static String UpdateCourse(CourseModel course,int course_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabCOURSE].[Course]
                        SET
                            [subject_id] = @subject_id,
                            [faculty_id]= @faculty_id,
                            [study_pr_id]= @study_pr_id,
                            [department_id]= @department_id,
                            [course_name]=@course_name,
                            [course_study_type]=@course_study_type,
                            [course_school_year]=@course_school_year,
                            [course_year] = @course_year,
                            [course_semester]= @course_semester,
                            [course_ESPB]=@course_ESPB,
                            [max_student_num]=@max_student_num,
                            [min_student_num]=@min_student_num,
                            [currently_active]=@currently_active,
                            [course_create_date]=@course_create_date,
                            [course_expiry_date]=@course_expiry_date,
                            [course_enrollment_policy] =@course_enrollment_policy      
                        WHERE
                            [course_id] = @course_id
                    ");
                    command.Parameters.AddWithValue("@course_id", course_id);
                    
                    FillData(command, course);
                    connection.Open();
                    command.ExecuteNonQuery();
                   
                    return "Course updated successfully"; // ovde je stajalo course.course_id.Value ali nece da radi sa tim PROVERI!!! 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
 
        public static String DeleteCourse(int course_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    
                    
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabCOURSE].[COURSE]
                        SET
                            [currently_active] = 0
                        WHERE
                            [course_id] = @course_id    
                    ");
 
                    command.Parameters.AddWithValue("@course_id", course_id);
                    
                    connection.Open();
                    command.ExecuteNonQuery();

                    return "Course deleted";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static String UpdateStudNum(CourseModel course,decimal max,decimal min,int course_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabCOURSE].[COURSE]
                        SET
                            [max_student_num]=@max_student_num,
                            [min_student_num]=@min_student_num
                                  
                        WHERE
                            [course_id] = @course_id
                    ");
                    command.Parameters.AddWithValue("@course_id", course_id);
                    command.Parameters.AddWithValue("@max_student_num", max);
                    command.Parameters.AddWithValue("@min_student_num", min);
                    
                    //FillData(command, course);
                    connection.Open();
                    command.ExecuteNonQuery();

                   
                    return "Number of students updated!";
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}