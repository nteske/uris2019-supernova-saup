using System;

namespace ServisKurs.Models
{   
    public class CourseModel
    {
        public decimal course_id{get;set;}
        public decimal subject_id{get;set;}
        public decimal faculty_id{get;set;}  
        public decimal study_pr_id{get;set;}
        public decimal department_id{get;set;}
        public string course_name{get;set;}
        public string course_study_type{get;set;}
        public decimal course_school_year{get;set;}
        public decimal course_year{get;set;}
        public decimal course_semester{get;set;}
        public decimal course_ESPB{get;set;}
        public decimal max_student_num{get;set;}
        public decimal min_student_num{get;set;}
        public bool currently_active{get;set;}
        public DateTime  course_create_date{get;set;}
        public DateTime  course_expiry_date{get;set;}
        public string course_enrollment_policy{get;set;}
    
 
        

 
        
 
 

    }
}