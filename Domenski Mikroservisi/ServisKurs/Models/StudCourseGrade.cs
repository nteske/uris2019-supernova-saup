namespace ServisKurs.Models
{
    public class StudCourseGrade
    {
        public decimal student_id{get;set;}
        public decimal course_id{get;set;}
        public decimal points{get;set;}
    }
}