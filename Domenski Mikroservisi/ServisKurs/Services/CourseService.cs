using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ServisKurs.DataAccess;
using ServisKurs.Models;

namespace ServisKurs.Services
{
    public interface ICourseService
    {
        List<CourseModel> GetAllCourses();
        CourseModel GetCourseById(int id);
        String CreateCourse(CourseModel course);
        String UpdateCourse(CourseModel course, int id);
        String DeleteCourse(int id);
        String CopyCourse(int id);
        String UpdateStudNum(decimal max,decimal min , int id);
    }

    public class CourseService : ICourseService
    {
        public List<CourseModel> GetAllCourses()
        {
            return CourseDB.GetAllCourses();
        }

        public CourseModel GetCourseById(int id)
        {
            return CourseDB.GetCourse(id);
        }

        public String CreateCourse(CourseModel course)
        {                  
            return CourseDB.CreateCourse(course);
        }

        public String UpdateCourse(CourseModel course, int id)
        {
            return CourseDB.UpdateCourse(course, id);
        }

        public String DeleteCourse(int id)
        {
            return CourseDB.DeleteCourse(id);
        }
        public String CopyCourse(int id)
        {
            CourseModel course = CourseDB.GetCourse(id);
            return CourseDB.CreateCourse(course);
        }
        public String UpdateStudNum(decimal max,decimal min , int id)
        {
            if(max<min)
                return "Max number of students must be greater then min number of students";
            CourseModel course = CourseDB.GetCourse(id);
            return CourseDB.UpdateStudNum(course,max,min,id);
        }
        
    }
}