using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ServisKurs.DataAccess;
using ServisKurs.Models;

namespace ServisKurs.Services
{
    public interface ITimetableService
    {
        List<TimetableModel> GetAllTimetables();
        TimetableModel GetTimetableById(decimal id_course,decimal id_teacher);
        String CreateTimetable(TimetableModel timetable);
        String UpdateTimetable(TimetableModel timetable, decimal id_course,decimal id_teacher);
        String DeleteTimetable(decimal id_course,decimal id_teacher);
    }

    public class TimetableService : ITimetableService
    {
        public List<TimetableModel> GetAllTimetables()
        {
            return TimetableDB.GetAllTimetables();
        }

        public TimetableModel GetTimetableById(decimal id_course,decimal id_teacher)
        {
            return TimetableDB.GetTimetableById(id_course,id_teacher);
        }

        public String CreateTimetable(TimetableModel timetable)
        {                  
            return TimetableDB.CreateTimetable(timetable);
        }
        
        public String UpdateTimetable(TimetableModel timetable, decimal id_course,decimal id_teacher)
        {
            return TimetableDB.UpdateTimetable(timetable, id_course,id_teacher);
        }

        public String DeleteTimetable(decimal id_course,decimal id_teacher)
        {
            return TimetableDB.DeleteTimetable(id_course,id_teacher);
        }
    }
}