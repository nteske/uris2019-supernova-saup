using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ServisKurs.DataAccess;
using ServisKurs.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace ServisKurs.Services
{
    
    public interface IStudCourseService
    {
        List<StudCourseModel> GetAllStudCourses();
        StudCourseModel GetStudCourseById(decimal id_course,decimal id_stud,decimal id_teacher);
        String CreateStudCourse(StudCourseModel studcourse);
        String UpdateStudCourse(StudCourseModel studcourse, decimal id_course,decimal id_stud,decimal id_teacher);
        String DeleteStudCourse(decimal id_course,decimal id_stud,decimal id_teacher);
        String UpdateStudGrade(StudCourseGrade studcoursegrade,int id_teacher);

    }
    public class StudCourseService : IStudCourseService
    {
        public List<StudCourseModel> GetAllStudCourses() {
            
            return StudCourseDB.GetAllStudCourses();

        }

        public StudCourseModel GetStudCourseById(decimal id_course,decimal id_stud,decimal id_teacher) {
            
            return StudCourseDB.GetStudCourseById(id_course,id_stud,id_teacher);
           
        }
        public String CreateStudCourse(StudCourseModel studcourse) {

            return StudCourseDB.CreateStudCourse(studcourse);

        }
        public String UpdateStudCourse(StudCourseModel studcourse, decimal id_course,decimal id_stud,decimal id_teacher) {

            return StudCourseDB.UpdateStudCourse(studcourse, id_course,id_stud,id_teacher);

        }
        public String DeleteStudCourse(decimal id_course,decimal id_stud,decimal id_teacher){
            
            return StudCourseDB.DeleteStudCourse(id_course,id_stud,id_teacher);

        }
        public String UpdateStudGrade(StudCourseGrade studcoursegrade,int id_teacher)
        {
            using(var client = new HttpClient()) 
            {
                var response = client.GetAsync("http://localhost:7005/api/GradeByPoints/"+ studcoursegrade.points).Result;
                 var result=response.Content.ReadAsStringAsync().Result;
                 var s =  JsonConvert.DeserializeObject(result);
                 Console.WriteLine(s);
                
                return StudCourseDB.UpdateGrade(studcoursegrade,id_teacher,Convert.ToInt32(s));
               
            }

        }
    }
}