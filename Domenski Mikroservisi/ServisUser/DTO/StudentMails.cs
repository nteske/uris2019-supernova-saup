namespace AuthMicroService.DTO
{
    public class StudentMails
    {
    public string[] Mails{get;set;}
     public string To{get;set;}
     public string Subject{get;set;}
     public string Body{get;set;}   
    }
}