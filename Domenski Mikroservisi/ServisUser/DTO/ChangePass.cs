namespace AuthMicroService.DTO
{
    public class ChangePass
    {
        public string OldPassword{get;set;}
        public string NewPassword{get;set;}
    }
}