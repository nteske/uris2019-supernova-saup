using System;
namespace AuthMicroService.DTO
{
    public class StudentObject
    {
        public decimal Users_id{get;set;}
        public string Username{get;set;}
        public string Email{get;set;}
        public string Password{get;set;}
        public string Jmbg{get;set;}
        public string First_name{get;set;}
        public string Last_name{get;set;}
        public DateTime Birth_date{get;set;}
        public string Gender{get;set;}
        public string Address{get;set;}
        public string City{get;set;}
        public string Country{get;set;}
        public string Mobile_phone{get;set;}
        public string Role{get;set;}
        public decimal Student_id{get;set;}
        public decimal Year_of_study{get;set;}
        public string Financing_method{get;set;}
        public decimal Times_taken{get;set;}
        public string Student_study_types{get;set;}
        public decimal _enrollment_year{get;set;}
    }
}