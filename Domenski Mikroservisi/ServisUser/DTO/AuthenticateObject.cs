//using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;
namespace AuthMicroService.DTO
{
    public class AuthenticateObject
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}