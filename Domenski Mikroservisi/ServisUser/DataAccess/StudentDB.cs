using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using AuthMicroService.Models;
using AuthMicroService.DTO;

namespace AuthMicroService.DataAccess
{
    public class StudentDB
    {
        private static StudentObject ReadRow(SqlDataReader reader)
        {
            StudentObject retVal = new StudentObject();
retVal.Users_id = (decimal)reader["users_id"];
            retVal.Username = reader["username"] as string;
            retVal.Email = reader["email"] as string;
            retVal.Password = reader["password"] as string;
            retVal.Role = reader["role"] as string;
            retVal.Jmbg = reader["jmbg"] as string;
            retVal.First_name = reader["first_name"] as string;
            retVal.Last_name = reader["last_name"] as string;
            retVal.Birth_date = (DateTime)reader["birth_date"];
            retVal.Gender = reader["gender"] as string;
            retVal.Address = reader["address"] as string;
            retVal.City = reader["city"] as string;
            retVal.Country = reader["country"] as string;
            retVal.Mobile_phone = reader["mobile_phone"] as string;
            retVal.Student_id = (decimal)reader["student_id"];
            retVal.Year_of_study = (decimal)reader["year_of_study"];
            retVal.Financing_method = reader["financing_method"] as string;
            retVal.Times_taken = (decimal)reader["times_taken"];
            retVal.Student_study_types = reader["student_study_type"] as string;
            retVal._enrollment_year = (decimal)reader["_enrollment_year"];
            return retVal;
        }
          private static void FillData(SqlCommand command, Student student)
        {
            command.Parameters.AddWithValue("@student_id",  student.Student_id);
            command.Parameters.AddWithValue("@year_of_study",  student.Year_of_study);
            command.Parameters.AddWithValue("@financing_method",  student.Financing_method);
            command.Parameters.AddWithValue("@times_taken",  student.Times_taken);
            command.Parameters.AddWithValue("@student_study_type",  student.Student_study_types);
            command.Parameters.AddWithValue("@_enrollment_year",  student._enrollment_year);

        }

         private static decimal ReadId(SqlDataReader reader)
        {
            return (decimal)reader["student_id"];
        }
        private static string AllColumnSelect
        {
            get
            {
                return @"
                [ellabUSER].[USERS].[users_id],
	                [ellabUSER].[USERS].[username],
	                [ellabUSER].[USERS].[email],
                    [ellabUSER].[USERS].[password],
                    [ellabUSER].[USERS].[role],
                    [ellabUSER].[USERS].[jmbg],
                    [ellabUSER].[USERS].[first_name],
                    [ellabUSER].[USERS].[last_name],
                    [ellabUSER].[USERS].[birth_date],
	                [ellabUSER].[USERS].[gender],
                    [ellabUSER].[USERS].[address],
                    [ellabUSER].[USERS].[city],
                    [ellabUSER].[USERS].[country],
                    [ellabUSER].[USERS].[mobile_phone],
                    [ellabUSER].[STUDENT].[student_id],
                    [ellabUSER].[STUDENT].[year_of_study],
                    [ellabUSER].[STUDENT].[financing_method],
                    [ellabUSER].[STUDENT].[times_taken],
                    [ellabUSER].[STUDENT].[student_study_type],
                    [ellabUSER].[STUDENT].[_enrollment_year]

                ";
            }
        }

        public static string CreateStudent(Student student)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabUSER].[STUDENT]
                        (
                            [student_id],
                            [year_of_study],
                            [financing_method],
                            [times_taken],
                            [student_study_type],
                            [_enrollment_year]                       
                        )
                        VALUES
                        (
                            @student_id,
                            @year_of_study,
                            @financing_method,
                            @times_taken,
                            @student_study_type,
                            @_enrollment_year
                        )
                                             
                    ";
                    FillData(command, student);
                    connection.Open();
 
                    decimal id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = ReadId(reader);
                        }
                    }
                   
 
                    return "Student created!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<StudentObject> GetStudents()
        {
            try
            {
                List<StudentObject> retVal = new List<StudentObject>();

                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabUSER].[STUDENT] inner join [ellabUSER].[USERS] on ([ellabUSER].[STUDENT].student_id=[ellabUSER].[USERS].users_id)
                    ", AllColumnSelect);
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(ReadRow(reader));
                        }
                    }
                }

                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}