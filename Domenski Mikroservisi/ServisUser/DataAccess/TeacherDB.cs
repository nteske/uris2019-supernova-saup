using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using AuthMicroService.Models;
using AuthMicroService.DTO;

namespace AuthMicroService.DataAccess
{
    public class TeacherDB
    {
        private static TeacherObject ReadRow(SqlDataReader reader)
        {
            TeacherObject retVal = new TeacherObject();
            retVal.Users_id = (decimal)reader["users_id"];
            retVal.Username = reader["username"] as string;
            retVal.Email = reader["email"] as string;
            retVal.Password = reader["password"] as string;
            retVal.Role = reader["role"] as string;
            retVal.Jmbg = reader["jmbg"] as string;
            retVal.First_name = reader["first_name"] as string;
            retVal.Last_name = reader["last_name"] as string;
            retVal.Birth_date = (DateTime)reader["birth_date"];
            retVal.Gender = reader["gender"] as string;
            retVal.Address = reader["address"] as string;
            retVal.City = reader["city"] as string;
            retVal.Country = reader["country"] as string;
            retVal.Mobile_phone = reader["mobile_phone"] as string;
            retVal.Teacher_id = (decimal)reader["teacher_id"];
            retVal.Employment_date = (DateTime)reader["employment_date"];
            retVal.Work_status = reader["work_status"] as string;
            retVal.Salary = (decimal)reader["salary"];
            return retVal;
        }
          private static void FillData(SqlCommand command, Teacher teacher)
        {
            command.Parameters.AddWithValue("@teacher_id", teacher.Teacher_id);
            command.Parameters.AddWithValue("@employment_date",  teacher.Employment_date);
            command.Parameters.AddWithValue("@work_status",  teacher.Work_status);
            command.Parameters.AddWithValue("@salary",  teacher.Salary);
        }

         private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["teacher_id"];
        }
        private static string AllColumnSelect
        {
            get
            {
                return @"
                [ellabUSER].[USERS].[users_id],
	                [ellabUSER].[USERS].[username],
	                [ellabUSER].[USERS].[email],
                    [ellabUSER].[USERS].[password],
                    [ellabUSER].[USERS].[role],
                    [ellabUSER].[USERS].[jmbg],
                    [ellabUSER].[USERS].[first_name],
                    [ellabUSER].[USERS].[last_name],
                    [ellabUSER].[USERS].[birth_date],
	                [ellabUSER].[USERS].[gender],
                    [ellabUSER].[USERS].[address],
                    [ellabUSER].[USERS].[city],
                    [ellabUSER].[USERS].[country],
                    [ellabUSER].[USERS].[mobile_phone],
                    [ellabUSER].[TEACHER].[teacher_id],
                    [ellabUSER].[TEACHER].[employment_date],
                    [ellabUSER].[TEACHER].[work_status],
                    [ellabUSER].[TEACHER].[salary]

                ";
            }
        }

        public static string CreateTeacher(Teacher teacher)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabUSER].[TEACHER]
                        (
                            [teacher_id],
                            [employment_date],
                            [work_status],
                            [salary]                         
                        )
                        VALUES
                        (
                            @teacher_id,
                            @employment_date,
                            @work_status,
                            @salary
                        )
                                             
                    ";
                    FillData(command, teacher);
                    connection.Open();
 
                    decimal id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = ReadId(reader);
                        }
                    }
                   
 
                    return "Teacher created!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<TeacherObject> GetTeachers()
        {
            try
            {
                List<TeacherObject> retVal = new List<TeacherObject>();

                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabUSER].[TEACHER] inner join [ellabUSER].[USERS] on ([ellabUSER].[TEACHER].teacher_id=[ellabUSER].[USERS].users_id)
                    ", AllColumnSelect);
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(ReadRow(reader));
                        }
                    }
                }

                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}