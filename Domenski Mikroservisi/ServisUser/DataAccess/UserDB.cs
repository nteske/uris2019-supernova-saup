﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using AuthMicroService.Models;
using AuthMicroService.Helpers;
namespace AuthMicroService.DataAccess
{
    public class UserDB
    {

        private static User ReadRow(SqlDataReader reader)
        {
            User retVal = new User();

            retVal.Users_id = (decimal)reader["users_id"];
            retVal.Username = reader["username"] as string;
            retVal.Email = reader["email"] as string;
            retVal.Password = reader["password"] as string;
            retVal.Role = reader["role"] as string;
            retVal.Jmbg = reader["jmbg"] as string;
            retVal.First_name = reader["first_name"] as string;
            retVal.Last_name = reader["last_name"] as string;
            retVal.Birth_date = (DateTime)reader["birth_date"];
            retVal.Gender = reader["gender"] as string;
            retVal.Address = reader["address"] as string;
            retVal.City = reader["city"] as string;
            retVal.Country = reader["country"] as string;
            retVal.Mobile_phone = reader["mobile_phone"] as string;
            return retVal;
        }

        private static decimal ReadId(SqlDataReader reader)
        {
            return (decimal)reader["users_id"];
        }

        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [ellabUSER].[USERS].[users_id],
	                [ellabUSER].[USERS].[username],
	                [ellabUSER].[USERS].[email],
                    [ellabUSER].[USERS].[password],
                    [ellabUSER].[USERS].[role],
                    [ellabUSER].[USERS].[jmbg],
                    [ellabUSER].[USERS].[first_name],
                    [ellabUSER].[USERS].[last_name],
                    [ellabUSER].[USERS].[birth_date],
	                [ellabUSER].[USERS].[gender],
                    [ellabUSER].[USERS].[address],
                    [ellabUSER].[USERS].[city],
                    [ellabUSER].[USERS].[country],
                    [ellabUSER].[USERS].[mobile_phone]
                ";
            }
        }
       internal static string UpdatePassword(String password,int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    password=ExtensionMethods.GetStringSha256Hash(password);
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabUSER].[USERS]
                        SET
	                        [password]=@Password 
                        WHERE
                            [users_id] = @Users_id
                    ");
                    command.Parameters.AddWithValue("@Password", password);

                    command.Parameters.AddWithValue("@Users_id", (decimal)id);
                    
                    connection.Open();
                    command.ExecuteNonQuery();
                   
                    return "Password changed successfully"; 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        internal static string UpdateUser(User user,int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabUSER].[USERS]
                        SET
	                        [username]=@Username,
                            [email]=@Email,
                            [role]=@Role,
                            [address]=@Address,
                            [city]=@City,
                            [country]=@Country,
                            [mobile_phone]=@Mobile_phone   
                        WHERE
                            [users_id] = @Users_id
                    ");
                    command.Parameters.AddWithValue("@Username", user.Username);
                    command.Parameters.AddWithValue("@Email", user.Email);
                    command.Parameters.AddWithValue("@Role", user.Role);
                    command.Parameters.AddWithValue("@Address", user.Address);
                    command.Parameters.AddWithValue("@City", user.City);
                    command.Parameters.AddWithValue("@Country", user.Country);
                    command.Parameters.AddWithValue("@Mobile_phone", user.Mobile_phone);

                    command.Parameters.AddWithValue("@Users_id", (decimal)id);
                    
                    connection.Open();
                    command.ExecuteNonQuery();
                   
                    return "User updated successfully"; 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        /*
       private static void FillData(SqlCommand command, User user)
       {
           command.Parameters.Add("@Id", SqlDbType.Int, user.Id);
           command.Parameters.Add("@Name", SqlDbType.NVarChar, user.Name);
           command.Parameters.Add("@Email", SqlDbType.NVarChar, user.Email);
           command.Parameters.Add("@Phone", SqlDbType.NVarChar, user.Phone);
           command.Parameters.Add("@UserTypeId", SqlDbType.Int, user.UserTypeId);
           command.Parameters.Add("@Active", SqlDbType.Bit, user.Active);
       }

       private static object CreateLikeQueryString(string str)
       {
           return str == null ? (object)DBNull.Value : "%" + str + "%";
       }
*/
        public static List<User> GetUsers()
        {
            try
            {
                List<User> retVal = new List<User>();

                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabUSER].[USERS]
                    ", AllColumnSelect);
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            retVal.Add(ReadRow(reader));
                        }
                    }
                }

                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         public static User GetUser(decimal users_id)
        {
            try
            {
                User retVal = null;
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabUSER].[USERS]
                        WHERE
                            [users_id] = @User_id
 
                    ", AllColumnSelect);
 
                    command.Parameters.AddWithValue("@User_id", users_id);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            retVal = ReadRow(reader);
                        }
                    }
                }
 
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static decimal CreateUser(User user)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    user.Password=ExtensionMethods.GetStringSha256Hash(user.Password);
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabUSER].[USERS]
                        (
	                            [username],
	                            [email],
                                [password],
                                [role],
                                [jmbg],
                                [first_name],
                                [last_name],
                                [birth_date],
	                            [gender],
                                [address],
                                [city],
                                [country],
                                [mobile_phone]                         
                        )
                        VALUES
                        (
	                        @username,
	                        @email,
                            @password,
                            @role,
                            @jmbg,
                            @first_name,
                            @last_name,
                            @birth_date,
	                        @gender,
                            @address,
                            @city,
                            @country,
                            @mobile_phone
                        )

                       SELECT scope_identity();
 
                    ";
                    FillData(command, user);
                    connection.Open();
 
                    decimal id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = Convert.ToDecimal(reader[0]);
                        }
                    }
                   
 
                    return id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         public static User GetUser(string email, string password)
        {
            try
            {
                User retVal = null;
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabUSER].[USERS]
                        WHERE
                            [email] = @Email AND
                            [password] = @Password
 
                    ", AllColumnSelect);
 
                    command.Parameters.AddWithValue("@Email", email);
                    command.Parameters.AddWithValue("@Password", password);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
 
                            retVal = ReadRow(reader);
                        }
                    }
                }
 
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static void FillData(SqlCommand command, User user)
        {
            command.Parameters.AddWithValue("@username",  user.Username);
            command.Parameters.AddWithValue("@email",  user.Email);
            command.Parameters.AddWithValue("@password",  user.Password);
            command.Parameters.AddWithValue("@role", user.Role);
            command.Parameters.AddWithValue("@jmbg", user.Jmbg);
            command.Parameters.AddWithValue("@first_name", user.First_name);
            command.Parameters.AddWithValue("@last_name", user.Last_name);
            command.Parameters.AddWithValue("@birth_date", user.Birth_date);
            command.Parameters.AddWithValue("@gender", user.Gender);
            command.Parameters.AddWithValue("@address", user.Address);
            command.Parameters.AddWithValue("@city", user.City);
            command.Parameters.AddWithValue("@country", user.Country);
            command.Parameters.AddWithValue("@mobile_phone",user.Mobile_phone);
            
        }
    }
}