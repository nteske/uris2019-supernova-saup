using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AuthMicroService.Models;
using Microsoft.AspNetCore.Authorization;
using AuthMicroService.Services;
using System.Security.Claims;
using AuthMicroService.DTO;
namespace AuthMicroService.Controllers
{
    [Route("/api/teacher")]
    [ApiController]
    public class TeacherController: ControllerBase
    {
        private ITeacherService _teacherService;

        public TeacherController(ITeacherService teacherService)
        {
            _teacherService = teacherService;
        }
        [HttpPost("kreiranjenastavnika")]
        public string KreiranjeNastavnika([FromBody]TeacherObject model)
        {
            return _teacherService.CreateTeacher(model);
        }
        [HttpGet("getnastavniks")]
        public List<TeacherObject> GetNastavniks()
        {
            /*string tekst="id korisnika:";
            tekst+=HttpContext.Request.Headers["User_id"]+"\n";
            tekst+="Uloga:";
            tekst+=HttpContext.Request.Headers["Role"]+"\n";
            tekst+=_userService.GetAll().ToString();*/
        return _teacherService.GetAll();
        }
    }
}