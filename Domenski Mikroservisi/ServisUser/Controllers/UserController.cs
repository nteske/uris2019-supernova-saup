using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AuthMicroService.Models;
using Microsoft.AspNetCore.Authorization;
using AuthMicroService.Services;
using System.Security.Claims;
using AuthMicroService.DTO;
using AuthMicroService.Helpers;
namespace AuthMicroService.Controllers
{
  //  [Authorize]
    [Route("/api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
 
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
      //  [AllowAnonymous]
        [HttpPost("prijavljivanje")]
        public IActionResult Prijavljivanje([FromBody]AuthenticateObject model)
        {
            model.Password=ExtensionMethods.GetStringSha256Hash(model.Password);
            var user = _userService.AuthenticateAsync(model.Email, model.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        [HttpPost("kreiranjekorisnika")]
        public IActionResult KreiranjeKorisnika([FromBody]User model)
        {
            _userService.CreateUser(model);
            return Ok("User Created!");
        }


        [HttpPost("azurirajpodatke")]
        public IActionResult AzurirajLicnePodatke([FromBody]User model)
        {
            int id=Convert.ToInt32(HttpContext.Request.Headers["User_id"]);
            _userService.UpdateUser(model,id);
            return Ok("User Changed!");
        }
        [HttpPost("promenisifru")]
        public IActionResult PromeniSifru([FromBody]ChangePass model)
        {
            int id=Convert.ToInt32(HttpContext.Request.Headers["User_id"]);
            User u=_userService.GetById(id);
            model.OldPassword=ExtensionMethods.GetStringSha256Hash(model.OldPassword);
            var user = _userService.AuthenticateAsync(u.Email, model.OldPassword);

            if (user == null)
                return BadRequest(new { message = "Password is incorrect" });
            return Ok( _userService.UpdatePassword(model.NewPassword,id));
        }
        [HttpPost("resetujsifru")]
        public IActionResult ResetujSifru()
        {
            int id=Convert.ToInt32(HttpContext.Request.Headers["User_id"]);
            User u=_userService.GetById(id);
            string password=ExtensionMethods.RandomString(10);
            string hashpassword=ExtensionMethods.GetStringSha256Hash(password);
            _userService.UpdatePassword(hashpassword,id);
            _userService.SendMailForChanges(u,password);
            return Ok("Password sended!");
        }
        //[Authorize(Roles = Role.Admin)]
        [HttpGet("getkorisnike")]
        public List<User> GetKorisnike()
        {
            /*string tekst="id korisnika:";
            tekst+=HttpContext.Request.Headers["User_id"]+"\n";
            tekst+="Uloga:";
            tekst+=HttpContext.Request.Headers["Role"]+"\n";
            tekst+=_userService.GetAll().ToString();*/
        return _userService.GetAll();
        }

        [HttpGet("getkorisnik/{id}")]
        public IActionResult GetKorisnik(int id)
        {
 
           var user =  _userService.GetById(id);
            if (user == null)
                return NotFound();

            return Ok(user);
        }
    }
}    