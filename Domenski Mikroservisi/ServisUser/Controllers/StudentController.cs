using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AuthMicroService.Models;
using Microsoft.AspNetCore.Authorization;
using AuthMicroService.Services;
using System.Security.Claims;
using AuthMicroService.DTO;
namespace AuthMicroService.Controllers
{
    [Route("/api/student")]
    [ApiController]
    public class StudentController: ControllerBase
    {
        private IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }
        [HttpPost("kreiranjestudenta")]
        public string KreiranjeStudenta([FromBody]StudentObject model)
        {
            return _studentService.CreateStudent(model);
        }
        [HttpPost("posaljimailgrupistudenata")]
        public IActionResult PosaljiMailGrupiStudenata([FromBody]StudentMails model)
        {
             _studentService.SendMails(model);
            return Ok();
        }
        
        [HttpGet("getstudents")]
        public List<StudentObject> GetStudents()
        {
        return _studentService.GetAll();
        }
    }
}