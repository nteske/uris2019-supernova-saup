using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using AuthMicroService.Models;
using AuthMicroService.Helpers;
using System.Threading.Tasks;
using AuthMicroService.DataAccess;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using AuthMicroService.DTO;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace AuthMicroService.Services
{
    public interface IStudentService
    {
        string CreateStudent(StudentObject model);
        List<StudentObject> GetAll();
        void SendMails(StudentMails model);
    }
    public class StudentService : IStudentService
    {   
        public string CreateStudent(StudentObject model){
            User user=BuildUser(model);
            Student student=new Student();
            student.Year_of_study=model.Year_of_study;
            student.Financing_method=model.Financing_method;
            student.Times_taken=model.Times_taken;
            student.Student_study_types=model.Student_study_types;
            student._enrollment_year=model._enrollment_year;
            decimal userov_id=UserDB.CreateUser(user);
            student.Student_id=userov_id;
            return StudentDB.CreateStudent(student);
        }
        private User BuildUser(StudentObject model){
            User u=new User();
            u.Username=model.Username;
            u.Email=model.Email;
            u.Password=model.Password;
            u.Jmbg=model.Jmbg;
            u.First_name=model.First_name;
            u.Last_name=model.Last_name;
            u.Birth_date=model.Birth_date;
            u.Gender=model.Gender;
            u.Address=model.Address;
            u.City=model.City;
            u.Country=model.Country;
            u.Mobile_phone=model.Mobile_phone;
            u.Role=model.Role;
            return u;
        }
        public List<StudentObject> GetAll()
        {
            return  StudentDB.GetStudents();
        }
        public void SendMails(StudentMails model){
            foreach(string mail in model.Mails){
                model.To=mail;
                var myContent = JsonConvert.SerializeObject(model);
                var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var client = new HttpClient();
                var result = client.PostAsync("http://localhost:7001/api/Email", byteContent).Result;
            }
        }

    }
}