using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using AuthMicroService.Models;
using AuthMicroService.Helpers;
using System.Threading.Tasks;
using AuthMicroService.DataAccess;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using AuthMicroService.DTO;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace AuthMicroService.Services
{

    public interface IUserService
    {
        User AuthenticateAsync(string username, string password);
        List<User> GetAll();
        User GetById(int id);
        decimal CreateUser(User model);
        string UpdateUser(User user,int id);
        string UpdatePassword(string password,int id);
        void SendMailForChanges(User u,String password);
    }

    public class UserService : IUserService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications


        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public User AuthenticateAsync(string email, string password)
        {

             var user =UserDB.GetUser(email,password);

            // return null if user not found
            if (user == null)
                 return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new List<Claim>
                {
                      new Claim("User_id", user.Users_id.ToString()),
                      new Claim("Role" ,user.Role.ToString())
                },JwtBearerDefaults.AuthenticationScheme),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = "Bearer "+tokenHandler.WriteToken(token);

              return user.WithoutPassword();
        }
        public string UpdateUser(User user,int id){
            return UserDB.UpdateUser(user,id);
        }
        public string UpdatePassword(string password,int id){
            return UserDB.UpdatePassword(password,id);
        }

        public decimal CreateUser(User model){
            User user=BuildUser(model);
            return UserDB.CreateUser(user);

        }


        private User BuildUser(User model){
            User u=new User();
            u.Username=model.Username;
            u.Email=model.Email;
            u.Password=model.Password;
            u.Jmbg=model.Jmbg;
            u.First_name=model.First_name;
            u.Last_name=model.Last_name;
            u.Birth_date=model.Birth_date;
            u.Gender=model.Gender;
            u.Address=model.Address;
            u.City=model.City;
            u.Country=model.Country;
            u.Mobile_phone=model.Mobile_phone;
            u.Role=model.Role;
            return u;
        }
        public List<User> GetAll()
        {
            return  UserDB.GetUsers();
        }
        public void SendMailForChanges(User u,String password){
                StudentMails mails=new StudentMails();
                mails.To=u.Email;
                mails.Subject="New password";
                mails.Body="Ne password is: "+password;
                var myContent = JsonConvert.SerializeObject(mails);
                var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var client = new HttpClient();
                var result = client.PostAsync("http://localhost:7001/api/Email", byteContent).Result;
        }
        public User GetById(int id) 
        {
            var user = UserDB.GetUser((decimal)id);

            if (user == null)
            {
                return null;
            }

            return user.WithoutPassword();
        }
    }
}