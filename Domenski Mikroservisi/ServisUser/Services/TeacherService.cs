using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using AuthMicroService.Models;
using AuthMicroService.Helpers;
using System.Threading.Tasks;
using AuthMicroService.DataAccess;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using AuthMicroService.DTO;
namespace AuthMicroService.Services
{
    public interface ITeacherService
    {
        string CreateTeacher(TeacherObject model);
        List<TeacherObject> GetAll();
    }
    public class TeacherService:ITeacherService
    {
        public string CreateTeacher(TeacherObject model){
            User user=BuildUser(model);
            Teacher teacher=new Teacher();
            teacher.Employment_date=model.Employment_date;
            teacher.Work_status=model.Work_status;
            teacher.Salary=model.Salary;
            decimal userov_id=UserDB.CreateUser(user);
            teacher.Teacher_id=userov_id;
            return TeacherDB.CreateTeacher(teacher);
        }
        private User BuildUser(TeacherObject model){
            User u=new User();
            u.Username=model.Username;
            u.Email=model.Email;
            u.Password=model.Password;
            u.Jmbg=model.Jmbg;
            u.First_name=model.First_name;
            u.Last_name=model.Last_name;
            u.Birth_date=model.Birth_date;
            u.Gender=model.Gender;
            u.Address=model.Address;
            u.City=model.City;
            u.Country=model.Country;
            u.Mobile_phone=model.Mobile_phone;
            u.Role=model.Role;
            return u;
        }

        public List<TeacherObject> GetAll()
        {
            return  TeacherDB.GetTeachers();
        }

    }
}