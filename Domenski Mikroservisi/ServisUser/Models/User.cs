using System;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;

namespace AuthMicroService.Models
{
    public class User
    {
        public decimal Users_id{get;set;}        
        public string Username{get;set;}
        public string Email{get;set;}
        public string Password{get;set;}
        public string Jmbg{get;set;}
        public string First_name{get;set;}
        public string Last_name{get;set;}
        public DateTime Birth_date{get;set;}
        public string Gender{get;set;}
        public string Address{get;set;}
        public string City{get;set;}
        public string Country{get;set;}
        public string Mobile_phone{get;set;}
        public string Token{get;set;}
        public string Role{get;set;}
    }
}