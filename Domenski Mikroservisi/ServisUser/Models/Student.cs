namespace AuthMicroService.Models
{
    public class Student
    {
        public decimal Student_id{get;set;}
        public decimal Year_of_study{get;set;}
        public string Financing_method{get;set;}
        public decimal Times_taken{get;set;}
        public string Student_study_types{get;set;}
        public decimal _enrollment_year{get;set;}
    }
}