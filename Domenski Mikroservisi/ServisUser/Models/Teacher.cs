using System;
namespace AuthMicroService.Models
{
    public class Teacher
    {
        public decimal Teacher_id{get;set;}
        public DateTime Employment_date{get;set;}
        public string Work_status{get;set;}
        public decimal Salary{get;set;}
    }
}