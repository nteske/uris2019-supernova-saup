using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServisOcena.Models;

namespace ServisOcena.DataAccess
{
    public class GradeDB
    {
         private static GradeModel ReadRow(SqlDataReader reader)
        {
            GradeModel retVal = new GradeModel();
 
            retVal.grade_id = Convert.ToInt32(reader["grade_id"]);
            retVal.grade = Convert.ToInt32(reader["grade"]);
            retVal.min_points_num = Convert.ToInt32(reader["min_points_num"]);
            retVal.max_points_num = Convert.ToInt32(reader["max_points_num"]);
 
            return retVal;
        }
 
        private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["grade_id"];
        }
 
        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [GRADE].[grade_id],
                    [GRADE].[grade],
                    [GRADE].[min_points_num],
                    [GRADE].[max_points_num]
                ";
            }
        }
 
        private static void FillData(SqlCommand command, GradeModel gradeModel)
        {  
            //command.Parameters.AddWithValue("@grade_id", gradeModel.grade_id);
            command.Parameters.AddWithValue("@grade", gradeModel.grade);
            command.Parameters.AddWithValue("@min_points_num", gradeModel.min_points_num);
            command.Parameters.AddWithValue("@max_points_num", gradeModel.max_points_num);
        }
 
        private static object CreateLikeQueryString(string str)
        {
            return str == null ? (object)DBNull.Value : "%" + str + "%";
        }
 
        public static List<GradeModel> GetGrades()
        {
            try
            {
                List<GradeModel> listOfGrades = new List<GradeModel>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"SELECT * FROM
                            [ellabGRADE].[GRADE]
                    ", AllColumnSelect);
 
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listOfGrades.Add(ReadRow(reader));
                        }
                    }
                }
 
                return listOfGrades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     public static GradeModel GetGrade(int grade_id)
        {
            try
            {
                GradeModel grade = new GradeModel();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabGRADE].[GRADE]
                        WHERE
                            [grade_id] = @grade_id
                    ", AllColumnSelect);
                    command.Parameters.AddWithValue("@grade_id", grade_id);
 
 
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            grade = ReadRow(reader);
                        }
                    }
                }
                return grade;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static decimal GetGradeByPoints(decimal points)
        {
              try
            {
                decimal grade=0;
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            [grade]
                        FROM
                            [ellabGRADE].[GRADE]
                        WHERE
                            [min_points_num] <= @points and [max_points_num] >= @points
                    ");
                    command.Parameters.AddWithValue("@points", points);
 
 
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            grade =Convert.ToInt32(reader["grade"]);
                        }
                    }
                }
                return grade;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static String CreateGrade(GradeModel grade)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabGRADE].[GRADE]
                        (
                            [grade],
                            [min_points_num],
                            [max_points_num]              
                        )
                        VALUES
                        (
                            @grade,
                            @min_points_num,
                            @max_points_num
                        )
                    ";
                    FillData(command, grade);
                    connection.Open();

                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = ReadId(reader);
                        }
                    }
 
                    return "Grade created successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String DeleteGrade(int grade_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"DELETE FROM [ellabGRADE].[GRADE] WHERE [grade_id] = @grade_id");
                    command.Parameters.AddWithValue("@grade_id", grade_id);
                    connection.Open();
                    command.ExecuteNonQuery();
                }
               
                return "Grade deleted successfully!";
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String UpdateGrade(GradeModel grade, int grade_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        UPDATE
                            [ellabGRADE].[GRADE]
                        SET
                            [grade] = @grade,
                            [min_points_num] = @min_points_num,
                            [max_points_num] = @max_points_num
                        WHERE
                            [grade_id] = @grade_id
                    ");
                    command.Parameters.AddWithValue("@grade_id", grade_id);

                    FillData(command, grade);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return "Grade updated successfully!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}