using ServisOcena.DataAccess;
using ServisOcena.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;

namespace ServisOcena.Services
{
    public interface IGradeService 
    {
        List<GradeModel> GetAll();
        GradeModel GetGradeById(int id);
        String CreateGrade(GradeModel grade);
        String UpdateGrade(GradeModel grade, int id);
        String DeleteGrade(int id);
        decimal GetGradeByPoints(decimal points);
    }

    public class GradeService : IGradeService
    {
        public List<GradeModel> GetAll()
        {
            if(GradeDB.GetGrades().Count == 0)
                return null;
            return GradeDB.GetGrades();
        }

        public GradeModel GetGradeById(int id)
        {
            if(GradeDB.GetGrade(id) is null)
                return null;
            return GradeDB.GetGrade(id);
        }
        
        public String CreateGrade(GradeModel grade)
        {                  
            return GradeDB.CreateGrade(grade);
        }

        public String UpdateGrade(GradeModel grade, int id)
        {
            return GradeDB.UpdateGrade(grade, id);
        }

        public String DeleteGrade(int id)
        {
            return GradeDB.DeleteGrade(id);
        }
        public decimal GetGradeByPoints(decimal points) {
            return GradeDB.GetGradeByPoints(points);
        }

    }
}