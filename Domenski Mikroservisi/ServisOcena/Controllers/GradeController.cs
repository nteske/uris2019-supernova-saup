using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServisOcena.DataAccess;
using ServisOcena.Models;
using ServisOcena.Services;
using Microsoft.AspNetCore.Mvc;

namespace ServisOcena.Controllers
{
    [ApiController]
    public class GradeController : ControllerBase
    {
        private IGradeService _gradeService;

        public GradeController(IGradeService gradeService) {
            _gradeService = gradeService;
        }

        [Route("api/Grades")]
        [HttpGet]
        public List<GradeModel> GetGrades()
        {
            var grades = _gradeService.GetAll();
            return grades;
        }

        [Route("api/Grade/{id}")]
        [HttpGet]
        public GradeModel GetGrade(int id)
        {
            var grade = _gradeService.GetGradeById(id);
            return grade;
        }
        [Route("api/GradeByPoints/{points}")]
        [HttpGet]
        public decimal GetGradeByPoints(decimal points)
        {
            var grade = _gradeService.GetGradeByPoints(points);
            return grade;
        }

        [Route("api/Grade")]
        [HttpPost]
        public String CreateGrade([FromBody]GradeModel grade)
        {
            var text = _gradeService.CreateGrade(grade);
            return text;
        }

        [Route("api/Grade/{id}")]
        [HttpDelete]
        public String DeleteGrade(int id)
        {
            var text = _gradeService.DeleteGrade(id);
            return text;
        }

        [Route("api/Grade/modify/{id}")]
        [HttpPut]
        public String UpdateDepartment([FromBody]GradeModel grade, int id)
        {
            var text = _gradeService.UpdateGrade(grade, id);
            return text;
        }
    }
}