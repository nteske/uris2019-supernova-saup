namespace ServisOcena.Models
{
    public class GradeModel
    {
        public decimal grade_id {get; set;}

        public decimal grade {get; set;}

        public decimal min_points_num {get; set;}

        public decimal max_points_num {get; set;}
    }
}