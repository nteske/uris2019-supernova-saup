using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MaterialMicroservice.Models;

namespace MaterialMicroservice.DataAccess
{
    public class MaterialDB
    {
        private static Material ReadRow(SqlDataReader reader)
        {
            Material retVal = new Material();
 
            retVal.material_id = (decimal)reader["material_id"];
            retVal.user_id = (decimal)reader["user_id"];
            retVal.course_id = (decimal)reader["course_id"];
            retVal.upload_date_time = (DateTime)reader["upload_date_time"];
            retVal.path = reader["path"] as string;
            retVal.visible =(Byte) reader["visible"];
            return retVal;
        }
 
        private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["Id"];
        }
 
        private static string AllColumnSelect
        {
            get
            {
                return @"
                    [Material].[material_id],
                    [Material].[user_id],
                    [Material].[course_id],
                    [Material].[upload_date_time],
                    [Material].[path],
                    [Material].[visible]
                ";
            }
        }
 
        private static void FillData(SqlCommand command, Material material)
        {
            command.Parameters.AddWithValue("@material_id", material.material_id);
            command.Parameters.AddWithValue("@user_id", material.user_id);
            command.Parameters.AddWithValue("@course_id", material.course_id);
            command.Parameters.AddWithValue("@upload_date_time",material.upload_date_time);
            command.Parameters.Add("@path", SqlDbType.NVarChar,300, material.path);
            command.Parameters.AddWithValue("@visible",material.visible);
        }
 
        private static object CreateLikeQueryString(string str)
        {
            return str == null ? (object)DBNull.Value : "%" + str + "%";
        }
 
        public static List<Material> GetMaterials()
        {
            try
            {
                List<Material> retVal = new List<Material>();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabMATERIAL].[MATERIAL]
                    ", AllColumnSelect);
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            retVal.Add(ReadRow(reader));
                    }
                }
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
 
        public static Material GetMaterial(decimal materialID)
        {
            try
            {
                Material retVal = null;//new Material();
 
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabMATERIAL].[MATERIAL]
                        WHERE
                            [material_id] = @Id
                    ", AllColumnSelect);
                    command.Parameters.AddWithValue("@Id", materialID);
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            retVal = ReadRow(reader);
                    }
                }
                    
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
 
        public static Material CreateMaterial(Models.Material material)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabMATERIAL].[MATERIAL]
                        (
                            [user_id],
                            [course_id],
                            [upload_date_time],
                            [path],
                            [visible]                
                        )
                        VALUES
                        (
                            @user_id,
                            @course_id,
                            @upload_date_time,
                            @path,
                            @visible
                        )
                    ";
                    command.Parameters.AddWithValue("@user_id", material.user_id);
                    command.Parameters.AddWithValue("@course_id", material.course_id);
                    command.Parameters.AddWithValue("@upload_date_time", material.upload_date_time);
                    command.Parameters.AddWithValue("@path", material.path);
                    command.Parameters.AddWithValue("@visible", material.visible);

                //    FillData(command, material);
                    connection.Open();
 
                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            id = ReadId(reader);
                    }
                    return GetMaterial(id);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    


    public static Boolean CopyMaterials(decimal sourceId, decimal destinationId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabMATERIAL].[MATERIAL]
                        (
                            [user_id],
                            [course_id],
                            [upload_date_time],
                            [path],
                            [visible]                
                        )
                        SELECT user_id,@destinationId,upload_date_time,path,visible 
                        FROM [ellabMATERIAL].[MATERIAL] WHERE course_id=@sourceId
                    ";

                    command.Parameters.AddWithValue("@sourceId",sourceId);
                    command.Parameters.AddWithValue("@destinationId",destinationId);
                    connection.Open();
 
                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            id = ReadId(reader);
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }








        public static Material UpdateMaterial(Material material,decimal material_id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        UPDATE
                            [ellabMATERIAL].[MATERIAL]
                        SET
                            [user_id] = @user_id,
                            [course_id] = @course_id,
                            [upload_date_time] = @upload_date_time,
                            [path] = @path,                    
                            [visible] = @visible
                        WHERE
                            [material_id] = @material_id
                    ");
                    command.Parameters.AddWithValue("@material_id", material_id);
                    command.Parameters.AddWithValue("@user_id", material.user_id);
                    command.Parameters.AddWithValue("@course_id", material.course_id);
                    command.Parameters.AddWithValue("@upload_date_time", material.upload_date_time);
                    command.Parameters.AddWithValue("@path", material.path);
                    command.Parameters.AddWithValue("@visible", material.visible);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return GetMaterial(material.material_id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
 
        public static void DeleteMaterial(decimal materialID)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
 
                        DELETE FROM
                            [ellabMATERIAL].[MATERIAL]
                        WHERE
                            [material_id] = @Id    
                    ");
 
                    command.Parameters.AddWithValue("@Id", materialID);
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}