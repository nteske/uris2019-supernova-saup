using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MaterialMicroservice.Models;

namespace MaterialMicroservice.DataAccess
{
    public class MaterialTeamDB
    {
        private static MaterialTeam ReadRow(SqlDataReader reader)
        {
            MaterialTeam retVal = new MaterialTeam();
            retVal.material_id = (decimal)reader["material_id"];
            retVal.team_id = (decimal)reader["team_id"];
            return retVal;
        }
 
        private static int ReadId(SqlDataReader reader)
        {
            return (int)reader["Id"];
        }
 
        private static string AllColumnSelect{
            get{
                return @"
                    [MATERIAL_TEAM].[material_id],
                    [MATERIAL_TEAM].[team_id]
                ";
            }
        }
 
        public static List<MaterialTeam> GetMaterialTeams(){
            try{
                List<MaterialTeam> retVal = new List<MaterialTeam>();
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabMATERIAL].[MATERIAL_TEAM]
                    ", AllColumnSelect);
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            retVal.Add(ReadRow(reader));
                    }
                }
                return retVal;
            }
            catch (Exception ex){ throw ex; }
        }
 
        public static MaterialTeam GetMaterialTeam(decimal materialID){
            try{
                MaterialTeam retVal = null;
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        SELECT
                            {0}
                        FROM
                            [ellabMATERIAL].[MATERIAL_TEAM]
                        WHERE
                            [material_id] = @Id
                    ", AllColumnSelect);
                    command.Parameters.AddWithValue("@Id", materialID);
                    connection.Open();
 
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            retVal = ReadRow(reader);
                    }
                }
                    
                return retVal;
            }
            catch (Exception ex){ throw ex; }
        }
 
        public static MaterialTeam CreateMaterialTeam(Models.MaterialTeam materialteam){
            try{
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"
                       INSERT INTO [ellabMATERIAL].[MATERIAL_TEAM]
                        (
                            [material_id],
                            [team_id]          
                        )
                        VALUES
                        (
                            @material_id,
                            @team_id
                        )
                    ";
                    command.Parameters.AddWithValue("@team_id", materialteam.team_id);

                    connection.Open();
 
                    int id = 0;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            id = ReadId(reader);
                    }
                    return GetMaterialTeam(id);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
 
        public static MaterialTeam UpdateMaterialTeam(MaterialTeam materialTeam,decimal material_id){
            try{
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        UPDATE [ellabMATERIAL].[MATERIAL_TEAM]
                        SET [team_id] = @team_id
                        WHERE [material_id] = @material_id
                    ");
                    command.Parameters.AddWithValue("@team_id", materialTeam.team_id);
                    connection.Open();
                    command.ExecuteNonQuery();
                    return GetMaterialTeam(materialTeam.material_id);
                }
            }
            catch (Exception ex)  { throw ex; }
        }
 
        public static void DeleteMaterialTeam(decimal materialID){
            try{
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSQL"].ConnectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = String.Format(@"
                        DELETE FROM
                            [ellabMATERIAL].[MATERIAL_TEAM]
                        WHERE
                            [material_id] = @Id    
                    ");
                    command.Parameters.AddWithValue("@Id", materialID);
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex) { throw ex; }
        }
    }
}