using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaterialMicroservice.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MaterialMicroservice.Models;
using Microsoft.AspNetCore.Http;
using System.IO;


namespace MaterialMicroservice.Controllers
{
    [ApiController]
    [Route("/api/materialteam/")]
    public class MaterialTeamController
    {


        [Route("get")]
        [HttpGet]
        public List<MaterialTeam> Get()
        {
            return MaterialTeamDB.GetMaterialTeams();
        }

        [Route("get/{id}")]
        [HttpGet]
        public MaterialTeam GetMaterialTeamById(decimal id){
            return MaterialTeamDB.GetMaterialTeam(id);
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public void DeleteMaterialTeam(decimal id){
            MaterialTeamDB.DeleteMaterialTeam(id);

        }

        [Route("create/")]
        [HttpPost]
        public MaterialTeam CreateMaterialTeam([FromBody]MaterialTeam material)
        {                  
            return MaterialTeamDB.CreateMaterialTeam(material);
        }
 
        [Route("modify/{id}")]
        [HttpPut]
        public MaterialTeam UpdateMaterialTeam([FromBody]MaterialTeam material, decimal id)
        {
            return MaterialTeamDB.UpdateMaterialTeam(material, id);
        }



    }
}