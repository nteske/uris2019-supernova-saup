﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaterialMicroservice.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MaterialMicroservice.Models;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace MaterialMicroservice.Controllers
{
    [ApiController]
    [Route("/api/material/")]
    public class MaterialController : ControllerBase
    {

        [Route("get")]
        [HttpGet]
        public List<Material> Get()
        {
            return MaterialDB.GetMaterials();
        }

        [Route("get/{id}")]
        [HttpGet]
        public Material GetMaterialById(decimal id){
            return MaterialDB.GetMaterial(id);
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public void DeleteMaterial(decimal id){
            string path =GetMaterialById(id).path;
            Console.WriteLine(path);
            System.IO.File.Delete(path);

            MaterialDB.DeleteMaterial(id);

        }

        [Route("create/")]
        [HttpPost]
        public Material CreateMaterial([FromBody]Material material)
        {                  
            return MaterialDB.CreateMaterial(material);
        }
 
        [Route("modify/{id}")]
        [HttpPut]
        public Material UpdateMaterial([FromBody]Material material, decimal id)
        {
            return MaterialDB.UpdateMaterial(material, id);
        }


    [Route("upload")]
    [HttpPost]
    public ActionResult Upload([FromForm]Material std)
    {
        
        string path="../../public/files/";
        // Getting Image
        var file = std.file;

        // Saving Image on Server
        if (file.Length > 0) {
            using (var fileStream = new FileStream(path+file.FileName, FileMode.Create)) {
                file.CopyTo(fileStream);
            }
        }
        std.path=path+file.FileName;
        std.upload_date_time=DateTime.Now;
        CreateMaterial(std);
        return Ok(new { status = true, message = "File Uploaded Successfully"});
    }

    [Route("download/{id}")]
    [HttpGet]
    public IActionResult Download(decimal id)
    {
        string folder="../../public/files/";
        Material material=MaterialDB.GetMaterial(id);
        string path=material.path;
        var net = new System.Net.WebClient();
        var data = net.DownloadData(path);
        var content = new System.IO.MemoryStream(data);
        var contentType = "APPLICATION/octet-stream";
        string fileName=path.Substring(folder.Length);
        return File(content, contentType, fileName);
    }

    public class IDS {
        public decimal source {get;set;}
        public decimal destination {get;set;}
    }
    [Route("import/")]
    [HttpPost]
    public ActionResult ImportMaterials([FromBody]IDS ids){
        MaterialDB.CopyMaterials(ids.source,ids.destination);
        return Ok(new { status= true, message=ids.source.ToString()+" "+ids.destination.ToString()});
    }
    }
}
