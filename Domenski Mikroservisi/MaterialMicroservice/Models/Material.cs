using System;
using Microsoft.AspNetCore.Http;

namespace MaterialMicroservice.Models
{
    public class Material
    {
        public decimal  material_id {get;set;}
        public decimal user_id {get;set;}
        public decimal course_id {get;set;}
        public System.DateTime upload_date_time {get;set;}
        public string path {get;set;}
        public Byte visible {get;set;}
        public IFormFile file {get;set;}

    }
}