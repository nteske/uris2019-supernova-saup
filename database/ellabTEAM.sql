/*PODSEMA TEAM */

IF EXISTS (SELECT * FROM sys.schemas WHERE name = 'ellabTEAM')
BEGIN
if object_id('ellabTEAM.TEAM','U') is not null drop table ellabTEAM.TEAM
EXEC('DROP SCHEMA ellabTEAM')
END
go

CREATE SCHEMA ellabTEAM
go

create table ellabTEAM.TEAM(
team_id numeric(10) identity (1,1) primary key,
course_id numeric(15),
team_name varchar(20),
team_number_of_points numeric(3),
team_description varchar(200),
)
