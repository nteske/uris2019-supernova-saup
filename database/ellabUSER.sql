/*PODSEMA USER
Tabela se zove USERS a ne USER jer je USER rezervisana rec i moze da pravi problem*/

IF EXISTS (SELECT * FROM sys.schemas WHERE name = 'ellabUSER')
BEGIN
if object_id('ellabUSER.USERS','U') is not null drop table ellabUSER.USERS
if object_id('ellabUSER.TEACHER','U') is not null drop table ellabUSER.TEACHER
if object_id('ellabUSER.STUDENT','U') is not null drop table ellabUSER.STUDENT
EXEC('DROP SCHEMA ellabUSER')

END
go

CREATE SCHEMA ellabUSER
go




create table ellabUSER.USERS (
users_id numeric(10) primary key IDENTITY(1,1),
username  varchar(30) unique ,
email varchar(30) unique ,
password char(128), /* SHA-512 crypted password */
role varchar(30) check (role in ('ADMIN','STUDENT','TEACHER')),
jmbg char(13) unique,
first_name varchar(15),
last_name varchar(15),
birth_date date,
gender char  check (gender in ('M','Z')),
address varchar(30),
city varchar(30),
country varchar(30),
mobile_phone varchar(30) unique
)

create table ellabUSER.STUDENT(
student_id numeric(10) primary key,
year_of_study numeric(3),
financing_method char(20) check (financing_method in ('BUDZET','SAMOFINANSIRANJE')),
times_taken numeric(3),
student_study_type char(3) check (student_study_type in ('OAS','MAS','OSS','IOS','SAS','SSS','DAS')),
_enrollment_year numeric(5),
constraint FK_STUDENT_USERS_STUDENT_ID_USER_ID foreign key (student_id) references ellabUSER.USERS(users_id)
)

create table ellabUSER.TEACHER(
teacher_id numeric(10) primary key foreign key references ellabUSER.USERS(users_id),
employment_date date,
work_status varchar(20),
salary numeric(10),
)
