/*PODSEMA FACULTY */

IF EXISTS (SELECT * FROM sys.schemas WHERE name = 'ellabFACULTY')
BEGIN
IF (OBJECT_ID('ellabFACULTY.FK_STUDY_PROGRAM_DEPARTMENT_FACULTY_ID_DEPARTMENT_ID', 'F') IS NOT NULL)
    ALTER TABLE ellabFACULTY.STUDY_PROGRAM DROP CONSTRAINT FK_STUDY_PROGRAM_DEPARTMENT_FACULTY_ID_DEPARTMENT_ID;
IF (OBJECT_ID('ellabFACULTY.FK_SUBJECT_STUDY_PROGRAM_FACULTY_ID_DEPARTMENT_ID_STUDY_PR_ID', 'F') IS NOT NULL)
    ALTER TABLE ellabFACULTY.SUBJECT DROP CONSTRAINT FK_SUBJECT_STUDY_PROGRAM_FACULTY_ID_DEPARTMENT_ID_STUDY_PR_ID;
if object_id('ellabFACULTY.SUBJECT','U') is not null drop table ellabFACULTY.SUBJECT
if object_id('ellabFACULTY.STUDY_PROGRAM','U') is not null drop table ellabFACULTY.STUDY_PROGRAM
if object_id('ellabFACULTY.DEPARTMENT','U') is not null drop table ellabFACULTY.DEPARTMENT
if object_id('ellabFACULTY.FACULTY','U') is not null drop table ellabFACULTY.FACULTY
EXEC('DROP SCHEMA ellabFACULTY')

END
go

CREATE SCHEMA ellabFACULTY
go


create table ellabFACULTY.FACULTY (
faculty_id numeric(10) primary key IDENTITY(1,1),
faculty_name varchar(35),
faculty_address varchar(35),
faculty_city varchar(15),
faculty_country varchar(15),
university_name varchar(35),
faculty_phone varchar(30) unique,
faculty_email varchar(30) unique
)

create table ellabFACULTY.DEPARTMENT(
department_id numeric(15) IDENTITY(1,1),
faculty_id numeric(10) foreign key references ellabFACULTY.FACULTY(faculty_id),
department_name varchar(100),
constraint PK_DEPARTMENT_FACULTY_ID_DEPRTMENT_ID primary key (faculty_id,department_id)
)

create table ellabFACULTY.STUDY_PROGRAM(
study_pr_id numeric(15) IDENTITY (1,1),
faculty_id numeric(10),
department_id numeric(15),
study_pr_name varchar(100) UNIQUE,
study_pr_shortname char(3),
study_pr_semester_num numeric(2),
constraint FK_STUDY_PROGRAM_DEPARTMENT_FACULTY_ID_DEPARTMENT_ID foreign key (faculty_id,department_id) references ellabFACULTY.DEPARTMENT(faculty_id, department_id),
constraint PK_STUDY_PROGRAM_FACULTY_ID_DEPARTMENT_ID_STUDY_PR_ID primary key (faculty_id,department_id,study_pr_id)
)

create table ellabFACULTY.SUBJECT(
subject_id numeric(15) IDENTITY (1,1),
faculty_id numeric(10),
department_id numeric(15),
study_pr_id numeric(15),
subject_name varchar(30) unique,
subject_description varchar(200),
subject_short_name char(8) unique,
constraint FK_SUBJECT_STUDY_PROGRAM_FACULTY_ID_DEPARTMENT_ID_STUDY_PR_ID  foreign key (faculty_id,department_id,study_pr_id) REFERENCES ellabFACULTY.STUDY_PROGRAM (faculty_id,department_id,study_pr_id),
constraint PK_SUBJECT_FACULTY_ID_DEPARTMENT_ID_STUDY_PR_ID_SUBJECT_ID primary key (faculty_id,department_id,study_pr_id,subject_id)
)