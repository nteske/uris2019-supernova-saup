/*PODSEMA EXAM */
IF EXISTS (SELECT * FROM sys.schemas WHERE name = 'ellabCOURSE')
BEGIN
if object_id('ellabCOURSE.TIMETABLE','U') is not null drop table ellabCOURSE.TIMETABLE   
if object_id('ellabCOURSE.STUD_COURSE','U') is not null drop table ellabCOURSE.STUD_COURSE
if object_id('ellabCOURSE.COURSE','U') is not null drop table ellabCOURSE.COURSE
EXEC('DROP SCHEMA ellabCOURSE')
END
go

CREATE SCHEMA ellabCOURSE
go

create table ellabCOURSE.COURSE(
    course_id numeric(15) identity(1,1) primary key ,
    subject_id numeric(15),
    faculty_id numeric(10),
    study_pr_id numeric(15),
    department_id numeric(15),
    course_name varchar(25),
    course_study_type char(3) check (course_study_type in ('OAS','MAS','OSS','IOS','SAS','SSS','DAS')),
    course_school_year numeric(1),
    course_year numeric(1),
    course_semester numeric(2),
    course_ESPB numeric(4),
    max_student_num numeric(3),
    min_student_num numeric(2),
    currently_active tinyint check (currently_active in (0,1)),
    course_create_date date,
    course_expiry_date date,
    course_enrollment_policy varchar(50),
)

create table ellabCOURSE.STUD_COURSE(
student_id numeric(10),
course_id numeric(15) foreign key references ellabCOURSE.COURSE(course_id),
teacher_id numeric(10),
enrolment_date date,
quit_date date,
points numeric(3) default 0 check (points >=0 and points <=100),
grade numeric(2) default 5 check (grade >=5 and grade <=10),
active tinyint check (active in (0,1)),
team_role varchar(20),
team_date_in date,
team_date_out date,
constraint PK_STUD_COURSE_STUDENT_ID_COURSE_ID_TEACHER_ID primary key (/*faculty_id,department_id,study_pr_id,subject_id,*/course_id,student_id,teacher_id),
)

create table ellabCOURSE.TIMETABLE(
course_id numeric(15) foreign key references ellabCOURSE.COURSE(course_id),
teacher_id numeric(10),
lesson_day char(10) check (lesson_day in ('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday')),
classroom varchar(15),
building varchar(20),
start_time time,
end_time time,
constraint PK_TIMETABLE__FACULTY_ID_DEPARTMENT_ID_STUDY_PR_ID_SUBJECT_ID_COURSE_ID_TEACHER_ID primary key (course_id,teacher_id)
)