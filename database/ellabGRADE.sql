/*PODSEMA GRADE */

IF EXISTS (SELECT * FROM sys.schemas WHERE name = 'ellabGRADE')
BEGIN
if object_id('ellab.GRADE','U') is not null drop table ellab.GRADE
EXEC('DROP SCHEMA ellabGRADE')
END
go

CREATE SCHEMA ellabGRADE
go


create table ellabGRADE.GRADE(
grade_id numeric(2) primary key identity(1,1),
grade numeric(2),
min_points_num numeric(3),
max_points_num numeric(3)
)
