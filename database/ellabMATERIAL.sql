/*PODSEMA MATERIAL */

if object_id('ellabMATERIAL.MATERIAL_TEAM','U') is not null drop table ellabMATERIAL.MATERIAL_TEAM
if object_id('ellabMATERIAL.MATERIAL','U') is not null drop table ellabMATERIAL.MATERIAL


IF EXISTS (SELECT * FROM sys.schemas WHERE name = 'ellabMATERIAL')
BEGIN
EXEC('DROP SCHEMA ellabMATERIAL')
END
go

CREATE SCHEMA ellabMATERIAL
go


create table ellabMATERIAL.MATERIAL(
material_id numeric(5) primary key identity(1,1),
user_id numeric(10),
course_id numeric(15),
upload_date_time datetime,
path varchar(300),
visible tinyint check (visible in (0,1)) default 0
)

create table ellabMATERIAL.MATERIAL_TEAM(
material_id numeric(5) primary key foreign key references ellabMATERIAL.MATERIAL(material_id),
team_id numeric(10)
)