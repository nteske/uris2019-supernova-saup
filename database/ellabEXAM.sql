/*PODSEMA EXAM */



IF EXISTS (SELECT * FROM sys.schemas WHERE name = 'ellabEXAM')
BEGIN
if object_id('ellabEXAM.STUD_EXAM','U') is not null drop table ellabEXAM.STUD_EXAM
if object_id('ellabEXAM.EXAM','U') is not null drop table ellabEXAM.EXAM
EXEC('DROP SCHEMA ellabEXAM')
END
go

CREATE SCHEMA ellabEXAM
go


create table ellabEXAM.EXAM(
exam_id numeric(15) identity(1,1) primary key,
course_id numeric(15),
teacher_id numeric(10),
test_name varchar(20),
exam_name varchar(20),
exam_type varchar(20),
exam_period varchar(20),
exam_date date,
exam_time timestamp,
exam_is_required tinyint check (exam_is_required in (0,1)),
exam_min_points numeric(3),
exam_max_points numeric(3)
)

create table ellabEXAM.STUD_EXAM(
exam_id numeric(15) foreign key references ellabEXAM.EXAM(exam_id),
student_id numeric(10),
date_of_access date,
check_out_date date,
point_score numeric(3),
is_passed tinyint check (is_passed in (0,1)),
exam_times_taken numeric(10),
constraint PK_STUD_EXAM_EXAM_ID_STUDENT_ID primary key (exam_id,student_id)
)
